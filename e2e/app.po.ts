import { browser, element, by } from 'protractor';

export class CoreUIPage {
  navigateTo() {
    return browser.get('/app/login');
  }

  getLoginBtnText() {
    return element(by.id('button-login')).getText();
  }

  getByCss(selector) {
  	return element(by.css(selector));
  }

  getInputBySelectorAndIndex(selector, index){
  	let els = element.all(by.css(selector));
  	return els.get(index);
  }

  waitForRedirect(fragment){
  	 return browser.driver.wait(()=>{
		        return browser.driver.getCurrentUrl().then(url=>url.indexOf(fragment)>-1);
	    },2000);
  }
}
