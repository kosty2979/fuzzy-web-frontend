import { CoreUIPage } from './app.po';
import { browser, element, by } from 'protractor';

describe('core-ui App', function() {
  let page: CoreUIPage;
  

  beforeEach(() => {
    page = new CoreUIPage();
  	page.navigateTo();
  });

  it('should has "Login"', () => {
    expect<any>(page.getLoginBtnText()).toEqual('Login');
  });

  it('should redirect to /sales', function () {
  		let loginBtn = page.getByCss('#button-login');
  		page.getInputBySelectorAndIndex('.form-control', 0).sendKeys('apps@fuzzylabsresearch.com');
  		page.getInputBySelectorAndIndex('.form-control', 1).sendKeys('sTNOkNH1jYHq48DLZHFw');
  		
        loginBtn.click().then(()=>{
        	page.waitForRedirect('/app/sales')
        }).then(()=>{
        	expect(browser.driver.getCurrentUrl()).toContain('/app/sales');
        });
    });
  it('should not redirect to /sales', function () {
  		let loginBtn = page.getByCss('#button-login');
  		page.getInputBySelectorAndIndex('.form-control', 0).sendKeys('ThisUserNotExist');
  		page.getInputBySelectorAndIndex('.form-control', 1).sendKeys('PasswordForNotExistingUser');
  		
        loginBtn.click().then(()=>{
        	expect((page.getByCss('.card #error')).isPresent());
        });
    });
});
