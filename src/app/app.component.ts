import { Component } from '@angular/core';
import { Logger } from 'angular2-logger/core';

import { environment } from '../environments/environment';
const LOGGER_LEVEL = environment.loggerLevel;


@Component({
    selector: 'body',
    template: '<router-outlet></router-outlet>'
})
export class AppComponent {
  constructor( private logger: Logger) {
    this.logger.level = LOGGER_LEVEL;
  }
}
