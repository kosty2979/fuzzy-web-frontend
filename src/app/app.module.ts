import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { APP_BASE_HREF } from '@angular/common';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';
import { ProfileDropdownComponent } from './components/profile/profile-button.component';

import { ToasterModule } from 'angular2-toaster/angular2-toaster';
import { Logger, Options } from 'angular2-logger/core';

import { AlertModule } from 'ngx-bootstrap/alert';
import { ModalModule } from 'ngx-bootstrap/modal';

import { FacebookModule } from 'ngx-facebook';
// Routing Module
import { AppRoutingModule } from './app.routing';

// Layouts
import { BlankLayoutComponent } from './layouts/blank-layout/blank-layout.component';
import { FullLayoutComponent } from './layouts/full-layout/full-layout.component';
import { NotFoundErrorComponent } from './components/404/404.component';
import { InternalServerErrorComponent } from './components/500/500.component';

import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth/auth.service';
import { ApiHttpService } from './services/api-http/api-http.service';
import { TrackerService } from './services/sp-tracker.service';
import { GlobalErrorHandler } from './services/error-handler';


@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    AlertModule.forRoot(),
    FacebookModule.forRoot(),
    ToasterModule,
    ModalModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    BlankLayoutComponent,
    FullLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    ProfileDropdownComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    NotFoundErrorComponent,
    InternalServerErrorComponent,
  ],
  providers: [
    {
      provide: ErrorHandler, 
      useClass: GlobalErrorHandler
    },
    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    },
    {
      provide: APP_BASE_HREF,
      useValue: '/app'
    },
    TrackerService,
    AuthGuard,
    ApiHttpService,
    Logger,
    Options,
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
