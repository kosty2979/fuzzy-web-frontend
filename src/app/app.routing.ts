import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { BlankLayoutComponent } from './layouts/blank-layout/blank-layout.component';
import { FullLayoutComponent } from './layouts/full-layout/full-layout.component';

import { NotFoundErrorComponent } from './components/404/404.component';

import { AuthGuard } from './guards/auth.guard';


export const routes: Routes = [
  {
    path: '',
    // redirectTo: 'private',
    redirectTo: '',
    pathMatch: 'full',
  },
  {
    path: '',
    component: BlankLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './public/public.module#PublicModule'
      }
    ]
  },
  {
    path: '',
    component: FullLayoutComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        children: [
          {
            path: '',
            loadChildren: './private/private.module#PrivateModule'
          }
        ]
      }
    ]
  },
  {
    path: '**',
    component: NotFoundErrorComponent
  }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
