import {  Component,
          OnInit,
          Input,
          ViewChild,
          OnChanges,
          SimpleChange,
          ElementRef
           } from '@angular/core';
import { ReportService } from '../../services/report/report.service';
import { LineChart } from '../../models/line-chart.model';
import { Period } from '../../models/period.model';
import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { TrackerService } from '../../services/sp-tracker.service';
import { UserDataService } from '../../services/user-data/user-data.service';

import * as moment from 'moment';


@Component({
  selector: 'advertisement-chart',
  templateUrl: './advertisement-chart.component.html',
  styleUrls: ['./advertisement-chart.component.scss']
})
export class AdvertisementChartComponent implements OnInit, OnChanges {

  @Input() period: Period;
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  public Chart: LineChart = new LineChart( [
      { text: 'Cost', id: 'cost', type: 'currency' },
      { text: 'CPA', id: 'cpa', type: 'currency'  },
      { text: 'Revenue', id: 'revenue', type: 'currency'  },
      { text: 'Conversions', id: 'conversions', type: 'numeric' }
    ]
  );
  public currentTime: Array<string> = [];
  public time: string ;
  public activeType: any;
  public load: boolean = false;

  public segmentModel: number[];
  public segmentOptions: IMultiSelectOption[];
  public segmentTexts: IMultiSelectTexts;
  public segmentSettings: IMultiSelectSettings;

  public segments: Array<any> = [
    { id: 0, name: 'segment 1', value: 'segment1' },
    { id: 1, name: 'segment 2', value: 'segment2' }
  ];

  public series: string [] = ['facebook', 'adwords', 'twitter'];

  public baseAggr: { id: string, name: string }[] = [
    {id: 'hour', name: 'Hourly'},
    {id: 'day', name: 'Daily'},
    {id: 'week', name: 'Weekly'},
    {id: 'month', name: 'Monthly'}
  ];

  accId: any;

  constructor(
    private tracker: TrackerService,
    private reportService: ReportService,
    private userData: UserDataService
  ) {
    this.activeType = this.Chart.activeType;
    this.time = this.currentTime[0];
  };

  ngOnInit() {
    this.setSegment();
    this.currentTime = this.period.time;
    this.setChartFormat();
    this.userData.getData().subscribe(
      res => {
        this.accId = res['accountName'];
      }
    );
  };

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes['period']) {
      this.currentTime = this.period.time;
      if( this.currentTime.indexOf(this.time) === -1) { this.time = this.currentTime[0];}
      this.getChartData();
    }
  };

  public getChartData(typeEl?) {
    const data: any[] = [];
    this.series.forEach( ( item ) => {
      const tmp: {} = {
        type: this.activeType.id,
        aggregateBy:  this.getAggrValue(),
        accountId: this.accId,
        platform: item,
        segmentIds: this.getSelectedSegments(),
        startingFrom: this.period.startDate,
        endingWith: this.period.endDate
      };
      data.push( tmp );
    });

    this.reportService.getAdvertChartData(data).then((res: Response[]) => {

      let Labels: string[] = [];
      let DataSet: any[] = [];
      res.forEach((item: any, i: any) => {
          item = item.json();
          let data:any [] = [];
          let labels:string [] = [];
          item['data'].map(( series: { timestamp: string, value: string }) => {
            labels.push(series.timestamp);
            data.push({x: series.timestamp, y: series.value});
          });
          Labels = labels;
          DataSet.push({ data: data, label: this.series[i] });
      });

      this.Chart.labels = Labels;
      this.Chart.data = DataSet;
      if( this.Chart.labels.length !== 0 ) this.load = true;
    });

  };

  public getAggrValue() {
    const aggr: { id: string, name: string } = this.baseAggr.find((item) => { return item.name === this.time; } );
    return aggr.id;
  };

  public getSelectedSegments(): string [] {
    let tmp: any [] = [];
    if (!this.segmentModel) {
      this.segmentModel = [0];
    }
    this.segments.forEach( ( item:any ) => {
      if ( this.segmentModel.indexOf( item.id ) !== -1 ) {
        tmp.push( item.value );
      }
    });
    return tmp;
  };


  public setSegment() {
    let model: Array<number> = [0];
    let options: Array<any> = this.segments.map( ( item ) => {
      return { id: item.id, name: item.name };
   });

    this.segmentTexts = {
      checkAll: 'Select all',
      uncheckAll: 'Unselect all',
      defaultTitle: 'no selected',
      allSelected: 'All selected'
    };
    this.segmentSettings = {
      itemClasses: 'segment-select',
      containerClasses: 'segment-wrapper',
      checkedStyle: 'fontawesome',
      showCheckAll: true,
      showUncheckAll: true,
      dynamicTitleMaxItems: 2,
      displayAllSelectedText: true
    };

    this.segmentOptions = options;
    this.segmentModel = model;
  };

  public setChartFormat() {
    this.Chart.options.scales.yAxes[0].ticks = {
      beginAtZero: true,
      callback: (label, index, labels) => {
        return this.activeType.type === 'numeric' ? (Math.round(label * 10) / 10) : ('$' + Math.round(label * 10) / 10);
      },

    };
    this.Chart.options.scales.xAxes[0].ticks= {
      autoSkip : false,
      callback: function(value, index, values) {
        if( index === 0 || index === values.length-1 ) return value
      }
    };
    this.Chart.options.scales.xAxes[0].gridLines = {
      display : false,
    };

    this.Chart.options.tooltips = {
      mode: 'x',
      backgroundColor: '#F0F0F0',
      bodyFontColor: '#111',
      titleFontColor: '#111',
      borderColor: 'rgba(0,0,0,1)',
      borderWidth: '1',
      callbacks: {
        title:  (tooltipItem, data) => {
          return moment(tooltipItem[0].xLabel).format('DD MMMM YYYY, h:mm a');
        },
        label:  (tooltipItems, data) => {
          return ' ' + data.datasets[tooltipItems.datasetIndex].label  + ': ' +
            (this.activeType.type === 'numeric' ? tooltipItems.yLabel : ('$' + tooltipItems.yLabel));
        }
      }
    };
  };

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  };

  trackSelection(event, el: ElementRef, action, typeEl, err?){
    this.tracker.trackEvent(event, el, action, typeEl, err);
  }

  public typeSelected(event, el, action, typeEl) {

    this.trackSelection(event, el, action, typeEl,);

    if ( event.id !== this.activeType.id ) {
      this.activeType = this.Chart.type.find( (item) => {
        return item.id === event.id;
      });
      this.getChartData();
    }
  };

  public timeSelected(event, el, action, typeEl) {

    this.trackSelection(event, el, action, typeEl);

    if ( this.time !== event.text ) {
      this.time = event.text;
      this.getChartData();
    }
  };

  public segmentSelected(event, el, action, typeEl) {
    let text:string;
    if(event.length === 0){
      text = "Uselect All";
    }else if(event.length === this.segments.length){
      text = "Select All";
    }else {
      text = this.segments.map(function(segment) {
        if(event.indexOf(segment.id) !== -1) {
          return segment.name;
        }
      }).join(',');
    text = text.substr(0, text.length-1);
    }
    event.text = text

    this.trackSelection(event,  el, action, typeEl);

    this.getChartData();
  };


}
