import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdwordsAccountComponent } from './adwords-account.component';

describe('AdwordsAccountComponent', () => {
  let component: AdwordsAccountComponent;
  let fixture: ComponentFixture<AdwordsAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdwordsAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdwordsAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
