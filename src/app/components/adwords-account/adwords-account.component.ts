import { Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { ToasterModule, ToasterService, ToasterConfig } from 'angular2-toaster/angular2-toaster';
import { Logger } from 'angular2-logger/core';

import { ConfirmComponent } from '../confirm/confirm.component';
import { AccountsService } from '../../services/accounts/accounts.service';

import { environment } from '../../../environments/environment';


const ASSETS_BASE_URL = environment.assetsBaseUrl;


type Account = {
  'id': string,
  'name': string
};


@Component({
  selector: 'adwords-account',
  templateUrl: './adwords-account.component.html',
  styleUrls: ['./adwords-account.component.scss']
})
export class AdwordsAccountComponent implements OnInit, AfterViewInit {

  providerImagePath: string;

  confirmData: object;
  isConfirmed: boolean;

  form: FormGroup;

  selectedCustomerId: string;
  customerIdList: Account[];

  toasterConfig: ToasterConfig =
    new ToasterConfig({
      tapToDismiss: true,
      timeout: 10000,
      positionClass: 'toast-top-left'
    });

  @Input() connectionInfo: any;
  @ViewChild('adwordsAccountModal') adwordsAccountModal: ModalDirective;
  @ViewChild(ConfirmComponent) readonly confirm: ConfirmComponent;

  constructor(private formBuilder: FormBuilder, private accountsService: AccountsService,
    private toasterService: ToasterService, private logger: Logger,
    private router: Router, private activatedRoute: ActivatedRoute) {
    this.providerImagePath = `${ASSETS_BASE_URL}/images/adwords`;

    this.confirmData = {
      title: 'Warning: Suspend segment publishing',
      content: `Disconnecting the account will stop any segments from being published to it.`
    };
    this.isConfirmed = false;

    this.form = this.formBuilder.group({
      'code': [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(30),
          Validators.maxLength(60)
        ])
      ],
    });

    this.customerIdList = [];
    this.selectedCustomerId = '';
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      const ids: string = params['ids'];
      const email: string = params['email'];
      const error: string = params['error'];

      if (ids && email) {
        const ids_list = ids.split('_');

        for (const customer in ids_list) {
          if (ids_list.hasOwnProperty(customer)) {
            const values = ids_list[customer].split('|');

            this.customerIdList.push({
              'id': values[0],
              'name': values[1]
            })
          }
        }
        this.selectedCustomerId = this.customerIdList[0]['id'];

        this.connectionInfo['isConnected'] = false;
        this.connectionInfo['email'] = email;

        this.showModal();
      }

      if (error) {
        this.showErrorNotification(error);

        this.showModal();
        this.clearPath();
      }
    });
  }

  showModal(): void {
    this.adwordsAccountModal.show();
  }

  customerIdSelected(event): void {
    this.selectedCustomerId = event.target.value;
  }

  clearPath(): void {
    const url: string = this.router.url.substring(0, this.router.url.indexOf('?'));
    this.router.navigateByUrl(url);
  }

  showSuccessNotification(message: string): void {
    this.showNotification('success', message);
  }

  showInfoNotification(message: string): void {
    this.showNotification('info', message);
  }

  showErrorNotification(message: string): void {
    this.showNotification('error', message);
  }

  showNotification(status: string, message: string): void {
    this.toasterService.pop(status, 'Adwords connection', message);
  }

  onConnect(): void {
    this.accountsService
      .onGetCodeForAdwords()
      .subscribe(res => {
        const response = res.json();
        this.logger.log('adwords.onGetCodeForAdwords()', response);

        if ('url' in response) {
          window.location.replace(response['url']);
        }

        if ('error' in response) {

        }
      });
  }

  onBack(): void {
    this.customerIdList = [];
    this.selectedCustomerId = '';

    this.clearPath();
    this.showModal();
  }

  onConnectSendCustomerId(): void {
    this.accountsService
      .onConnectToAdwords(this.connectionInfo.email, this.selectedCustomerId)
      .subscribe(res => {
        const response = res.json();
        if ('error' in response) {
          this.logger.error('adwords.onConnectToAdwords()', response['error']);

          this.showErrorNotification(response['error']);
        } else {
          this.logger.log('adwords.onConnectToAdwords()', response);

          this.connectionInfo.id = response[0].id;
          this.connectionInfo.email = response[0].email;
          this.connectionInfo.accountId = response[0].account_id;
          this.connectionInfo.accountName = response[0].account_name;
          this.connectionInfo.isConnected = true;

          this.customerIdList = [];
          this.selectedCustomerId = '';

          this.showSuccessNotification('You connected to adwords!');
          this.clearPath();
          this.showModal();
        }
      });
  }

  onTestConnection(): void {
    this.showInfoNotification('Testing...');

    this.accountsService
      .onTestAdwordsConnection(this.connectionInfo.id)
      .subscribe(res => {
        const response = res.json();
        if ('totalNumEntries' in response) {
          this.logger.log('adwords.test()', response);

          this.showSuccessNotification('It works!');
        } else {
          this.logger.error('adwords.test()', response);

          this.showErrorNotification(
            'Something went wrong. Disconnect and try again.'
            );
        }
      });
  }

  onDisconnect(): void {
    if (!this.isConfirmed) {
      this.confirm.onShow();
      return;
    }

    this.accountsService
      .onDisconnectFromAdwords(this.connectionInfo.id)
      .subscribe(res => {
        const response = res.json();
        if (!!response) {
          this.logger.error('adwords.onConnectToAdwords()', response['error']);
        } else {
          this.logger.log('adwords.disconnect()', response);

          this.connectionInfo.id = '';
          this.connectionInfo.email = '';
          this.connectionInfo.accountId = '';
          this.connectionInfo.accountname = '';
          this.connectionInfo.isConnected = false;

          this.customerIdList = [];
          this.selectedCustomerId = '';

          this.showSuccessNotification(
            'You are disconnected from adwords account.'
            );
        }
      });
  }

  onConfirm(event): void {
    if (event) {
      this.isConfirmed = event;
    }

    this.onDisconnect();

    this.isConfirmed = false;
  }

}
