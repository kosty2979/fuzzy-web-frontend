import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent {

  @Input() data: any;
  @ViewChild('confirm') confirm: ModalDirective;
  @Output() prompt: EventEmitter<boolean> = new EventEmitter();
  private _onOkObservable: Observable<any> | null;
  private _onOkCallback: () => any | null;

  constructor() {
    this.data = {
      title: 'Modal Title',
      content: 'One fine body…'
    };
  }

  onShow(): void {
    this.confirm.show();
  }

  onHide(): void {
    this.confirm.hide();
    this.onOkObservable = null;
    this.onOkCallback = null;
  }

  onCancel(): boolean {
    this.prompt.emit(false);
    this.onHide();
    return false;
  }

  set onOkObservable(observable: Observable<any> | null) {
    this._onOkObservable = observable;
  }

  get onOkObservable(): Observable<any> {
      return this._onOkObservable;
  }

  set onOkCallback(callback: () => any) {
    this._onOkCallback = callback;
  }

  get onOkCallback(): () => any {
      return this._onOkCallback;
  }

  onOK(): Subscription | boolean {
    this.prompt.emit(true);
    if (this.onOkObservable) {
      this.onOkObservable.subscribe();
    }
    else if (this.onOkCallback) {
      this.onOkCallback();
    }
    this.onHide();
    return true;
  }
}
