import { Component, Input, OnInit, OnChanges, AfterViewInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { FacebookService, InitParams, LoginResponse, LoginOptions, LoginStatus } from 'ngx-facebook';
import { ToasterModule, ToasterService, ToasterConfig } from 'angular2-toaster/angular2-toaster';
import { Logger } from 'angular2-logger/core';

import { ConfirmComponent } from '../confirm/confirm.component';
import { AccountsService } from '../../services/accounts/accounts.service';

import { environment } from '../../../environments/environment';


const ASSETS_BASE_URL = environment.assetsBaseUrl;
const FACEBOOK_APP_ID = environment.facebookAppId;
const FACEBOOK_PERMISSIONS_LEVEL = 3;


type Account = {
  'id': string,
  'name': string
};


@Component({
  selector: 'facebook-account',
  templateUrl: './facebook-account.component.html',
  styleUrls: ['./facebook-account.component.scss']
})
export class FacebookAccountComponent implements OnInit {

  providerImagePath: string;
  confirmData: object;
  isConfirmed: boolean;

  scopes: string[];

  selectedAdAccountId: string;
  adAccountList: Account[];

  toasterConfig: ToasterConfig =
    new ToasterConfig({
      tapToDismiss: true,
      timeout: 10000,
      positionClass: 'toast-top-left'
    });

  @Input() connectionInfo: any;
  @ViewChild('facebookAccountModal') facebookAccountModal: ModalDirective;
  @ViewChild(ConfirmComponent) readonly confirm: ConfirmComponent;

  credentials: any;

  constructor(private accountsService: AccountsService, private router: Router,
    private fb: FacebookService, private toasterService: ToasterService,
    private logger: Logger) {
    this.providerImagePath = `${ASSETS_BASE_URL}/images/facebook.png`;

    this.confirmData = {
      title: 'Warning: Suspend segment publishing',
      content: `Disconnecting the account will stop any segments from being published to it.`
    };
    this.isConfirmed = false;

    this.scopes = ['public_profile', 'email', 'ads_management', 'ads_read']
    const initParams: InitParams = {
      appId: FACEBOOK_APP_ID,
      xfbml: true,
      cookie: false,
      version: 'v2.9'
    };
    this.fb.init(initParams);

    this.adAccountList = [];
    this.credentials = null;
  }

  ngOnInit() {

  }

  showModal() {
    this.facebookAccountModal.show();
  }

  onGetAdAccount() {
    if (!this.connectionInfo.isConnected) {

      this.login()
        .then((response: any) => {
          if (this.credentials) {
            this.getFacebookUserInfo();
          }
        });
    }
  }

  adAccountSelected(event) {
    this.selectedAdAccountId = event.target.value;
  }

  onConnect() {
    this.credentials.adAccountId = this.selectedAdAccountId;

    this.checkPermissions();
  }

  onTest() {
    this.toasterService.pop('info', 'Facebook connection',
      'Testing...'
      );

    this.accountsService
      .onTestFacebookConnection(this.connectionInfo.id)
      .subscribe(res => {
        const response = res.json();
        if (response[0].includes('data')) {
          this.logger.log('facebook.test()', response);

          this.toasterService.pop('success', 'Facebook connection', 'It works!');
        } else {
          this.logger.error('facebook.test()', response);

          this.toasterService.pop('error', 'Facebook connection',
            'Something went wrong. Disconnect and try again.'
            );
        }
      });
  }

  onDisconnect(): void {
    if (!this.isConfirmed) {
      this.confirm.onShow();
      return;
    }

    this.deleteConnection();
  }

  onConfirm(event): void {
    if (event) {
      this.isConfirmed = event;
    }

    this.onDisconnect();

    this.isConfirmed = false;
  }

  login(): Promise<any> {
    const loginOptions: LoginOptions = {
        scope: this.scopes.join(','),
        return_scopes: true,
        enable_profile_selector: true,
        auth_type: 'rerequest'
      };

    return this.fb.login(loginOptions)
      .then((response: LoginResponse) => {
        this.logger.log('facebook.login()', response);

        const permissions = response['authResponse']['grantedScopes'];

        this.credentials = { };
        this.credentials.accountId = response.authResponse.userID;
        this.credentials.accessToken = response.authResponse.accessToken;
      })
      .catch((error: any) => {
        this.logger.error('facebook.login()', error);
      });
  }

  logout(): Promise<any> {
    return this.fb.logout()
      .then((response: LoginResponse) => {
        this.logger.log('facebook.logout()', response);

        return response;
      })
      .catch((error: any) => {
        this.logger.error('facebook.logout()', error);
      });
  }

  getFacebookUserInfo(): Promise<any> {
    return this.fb.api('/me?fields=id,name,email', 'get')
      .then((response: any) => {
        this.logger.log('facebook.getFacebookUserInfo()', response);

        this.credentials.name = response['name'];
        this.credentials.email =  response['email'];

        this.getFacebookAdaccountInfo();

        return response;
      })
      .catch((error: any) => {
        this.logger.error('facebook.getFacebookUserInfo()', error);

      });
  }

  getFacebookAdaccountInfo(): Promise<any> {
    this.adAccountList = [];

    return this.fb.api('/me/adaccounts?fields=id,name', 'get')
      .then((response: any) => {
        this.logger.log('facebook.getFacebookAdaccountInfo()', response);

        for (const adAccount in response['data']) {
          if (response['data'].hasOwnProperty(adAccount)) {
            const element = response['data'][adAccount];

            this.adAccountList.push(element);
          }
        }
        this.selectedAdAccountId = this.adAccountList[0]['id'];

        if (this.adAccountList.length === 1) {
          this.onConnect();
        }

        return response;
      })
      .catch((error: any) => {
        this.logger.error('facebook.getFacebookAdaccountInfo()', error);
      });
  }

  checkPermissions(): Promise<any> {
    return this.fb.api('/' + this.credentials.adAccountId + '/users', 'get')
      .then((response: any) => {
        this.logger.log('facebook.checkPermissions()', response);

        if (!this.credentials.adAccountId) {
          this.toasterService.pop('warning', 'Facebook connection',
            'You should choose ad account id.'
            );
          return;
        }

        const permissions: Int16Array = response['data'][0]['permissions'];
        if (permissions.includes(FACEBOOK_PERMISSIONS_LEVEL) && this.credentials) {
          const adAccountName = this.getAdAccountName(this.credentials.adAccountId);

          this.addConnection(this.credentials.email, this.credentials.adAccountId,
            adAccountName, this.credentials.accessToken);
        } else {
          this.credentials = null;

          this.toasterService.pop('error', 'Facebook connection',
            'You should allow write app permissions to continue.'
            )
        }

        return response;
      })
      .catch((error: any) => {
        this.logger.error('facebook.checkPermissions', error);

        this.credentials = null;

        this.toasterService.pop('error', 'Facebook connection',
          'You should allow all app permissions to continue.'
          )
      });
  }

  addConnection(email: string, accountId: string, accountName: string, accessToken: string): void {
    this.accountsService
      .onConnectToFacebook(email, accountId, accountName, accessToken)
      .subscribe(res => {
        const response = res.json();
        this.logger.log('facebook.addConnection()', response);

        this.toasterService.pop('success', 'Facebook connection',
          'You connected to facebook.'
          );

        for (const connection in response) {
          if (response.hasOwnProperty(connection)) {
            const element = response[connection];

            this.connectionInfo.id = element.id;
            this.connectionInfo.email = element.email;
            this.connectionInfo.accountId = element.account_id;
            this.connectionInfo.accountName = element.account_name;
          }
        }
        this.connectionInfo.isConnected = true;
        this.credentials = null;

        this.logout();
      });
  }

  deleteConnection(): void {
    this.accountsService
      .onDisconnectFromFacebook(this.connectionInfo.id)
      .subscribe(res => {
        const response = res.json();
        this.logger.log('facebook.deleteConnection()', response);

        this.toasterService.pop('success', 'Facebook connection',
          'You disconnected from facebook.'
          );

        this.connectionInfo.id = '';
        this.connectionInfo.email = '';
        this.connectionInfo.accountId = '';
        this.connectionInfo.accountName = '';
        this.connectionInfo.isConnected = false;

        this.credentials = null;
        this.selectedAdAccountId = '';
      });
  }

  getAdAccountName(id: string): string {
    for (const adAccount in this.adAccountList) {
      if (this.adAccountList.hasOwnProperty(adAccount)) {
        const element = this.adAccountList[adAccount];

        if (element['id'] === id) {
          return element['name'];
        }
      }
    }

    return '';
  }
}
