import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleStorageComponent } from './google-storage.component';

describe('GoogleStorageComponent', () => {
  let component: GoogleStorageComponent;
  let fixture: ComponentFixture<GoogleStorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleStorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
