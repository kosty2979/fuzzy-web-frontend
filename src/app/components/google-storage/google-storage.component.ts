import { Component } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';

import { environment } from 'environments/environment';
import { CookieService } from 'app/services/cookie/cookie.service';
import { Observable } from "rxjs/Observable";
import { ImportTabComponent } from 'app/generic/components/imports/import-tab.component'
import { ApiHttpService } from "app/services/api-http/api-http.service";

@Component({
  selector: 'google-storage',
  templateUrl: './google-storage.component.html',
  styleUrls: ['./google-storage.component.scss']
})
export class GoogleStorageComponent extends ImportTabComponent {

  constructor(private formBuilder: FormBuilder, private apiHttpService: ApiHttpService) {
    super();
  }

  constructorFunction(): void {
    this.submitted = false;
    // this.uploaded = true;
    this.validationClasses = {
      'url': {
        'formGroup': {
          'has-success': false,

          'has-danger': false
        },
        'formControl': {
          'form-control-success': false,
          'form-control-danger': false
        }
      }
    };
    this.validationMessages = {
      'url': {
        'required': {
          'message': 'URL is required.'
        },
        'pattern': {
          'type': 'warning',
          'message': 'URL format must be <em>gs://bucket/name.ext</em>'
        }
      }
    };
    this.confirmData = {
      title: 'Warning: Import deletes current sales data',
      content: `Reimporting sales data will delete all existing sales data that was imported previously.`
    };
    this.confirmed = false;
    this.formErrors = [];
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      'type': 'gs',
      'url': [
        null,
        [
          Validators.required,
          Validators.pattern(/^(gs:\/\/)([\da-z\-]+)\/([\/\w \-]*)*\.([a-z\.]{2,6})$/)
        ]
      ],
      'isAuthorized': null,
      'schedule': this.formBuilder.group({
        'reimport': null,
        'type': {
          value: null,
          disabled: true
        }
      })
    });
    this.authorize = this.formBuilder.group({
      'accountId': null,
      'accessToken': null,
      'refreshToken': null
    });

    this.form
      .valueChanges
      .subscribe(() => this.onValueChanged());
    this.form
      .get('schedule.reimport')
      .valueChanges
      .subscribe(reimport => this.onReimportValueChanged(reimport));

    // this.onValueChanged();
  }

  onValueChanged(): void {
    if (!this.form) {
      return;
    }
    const form = this.form;
    this.formErrors = [];

    for (const field in this.validationMessages) {
      if (this.validationMessages.hasOwnProperty(field)) {
        const control = form.get(field);

        if (!control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {

            if (control.errors.hasOwnProperty(key)) {
              // this.formErrors[field] += messages[key];
              const error = messages[key];
              this.formErrors.push(error);
              control.markAsTouched();
            }
          }
        }
      }
    }

    const urlTouched = form.get('url').touched,
          urlValid = form.get('url').valid,
          urlSuccess = urlTouched && urlValid,
          urlDanger = urlTouched && !urlValid;

    this.validationClasses = {
      'url': {
        'formGroup': {
          'has-success': urlSuccess,
          'has-danger': urlDanger
        },
        'formControl': {
          'form-control-success': urlSuccess,
          'form-control-danger': urlDanger
        }
      }
    };
  }

  onImportButtonClick(): void {
    this.onValueChanged();
    const url_input = this.form.get('url');
    if (url_input.valid) {
      if (!this.confirmed) {
        this.confirm.onShow();
      } else {this.sendHttpImportRequest(this.prepareImportRequestParams());}
    }
  }

  onAuthorize() {
    let auth: boolean;
    const auth_status_request = this.apiHttpService.get_ApiImportsV1AuthGsStatus({}).subscribe(response => {
      if (response.status == 200) {
        auth = true;
        alert('You already authorized') // TODO: replace with custom alert
      } else {
        auth = false;
      }
    });
    const auth_subscription = Observable.timer(100, 500).subscribe(() => {
      if (auth_status_request.closed) {
        if (auth === true) {
          auth_subscription.unsubscribe()
        }
        else if (auth === false) {
          auth_subscription.unsubscribe();
          const token = CookieService.get('access_token');
          window.location.href = `${environment.apiUrl}/api/imports/${environment.apiVersion}/auth/gs?access_token=${token}`;
        }
      }
    });

  }

  private sendHttpImportRequest(request_params) {
      this.apiHttpService.post_ApiImportsV1Gs({'apiImportsV1Gs': request_params }).subscribe(res => {
        if (res.ok) {
          this.submitted = true;
        }
      });
  }

  onConfirm(event: boolean): void {
    if (event) {
      this.sendHttpImportRequest(this.prepareImportRequestParams());
      this.confirmed = event;
    }
  }
}
