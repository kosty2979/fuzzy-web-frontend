import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';

import { TrackerService } from '../../services/sp-tracker.service';

import { Period } from '../../models/period.model';


@Component({
  selector: 'period-select',
  templateUrl: './periodSelect.html',
  styleUrls: ['./periodSelect.scss']
})

export class PeriodSelectComponent implements OnInit {

  @ViewChild('periodModal') public periodModal: ModalDirective;
    
  // @ViewChild('periodSelect')periodSelect: any;

  @Output() setPeriod = new EventEmitter();
  selectedPeriod: { text: string, id: string };
  tmpPeriod: { text: string, id: string };
  period: { text: string, id: string }[];
  showStartPicker: boolean = false;
  showEndPicker: boolean = false;
  periodObj: Period;
  startDate: Date;
  endDate: Date;
  minDate: Date = new Date();
  maxDate: Date = new Date();
  apply: boolean = false;

  constructor(private tracker: TrackerService){}

  ngOnInit() {
    this.period = [{
      text: 'Custom',
      id: '0'
    }, {
      text: 'Today',
      id: '1'
    }, {
      text: 'Yesterday',
      id: '2'
    }, {
      text: 'This week(Mon - Today)',
      id: '3'
    }, {
      text: 'Last 30 days',
      id: '4'
    }, {
      text: 'This year',
      id: '5'
    }, {
      text: 'All time',
      id: '6'
    }];
    this.selectedPeriod = this.period[1];
    this.setPeriodObj();
  };
  
  trackSelection(event, el: ElementRef, action, type){
    this.tracker.trackEvent(event, el, action, type);
  }

  public periodSelected(event,  el, action, type) {
    this.trackSelection(event, el, action, type)
    
    if (event.id === '0') {
      this.tmpPeriod = this.selectedPeriod;
      this.selectedPeriod = event;
      this.periodModal.show();
    } else {
      this.tmpPeriod = event;
      if ( this.selectedPeriod.id !== event.id ) {
        this.selectedPeriod = event;
        this.setPeriodObj();
      }
    }
  };



  public applyPeriodModal() {
    this.apply = true;
    this.selectedPeriod = {text: moment(this.startDate).format('YYYY-MM-DD') + ' - ' +  moment(this.endDate).format('YYYY-MM-DD'), id: '0'};
    this.setPeriodObj();
    this.periodModal.hide();
  };

  public resetModal() {
    this.showStartPicker = false;
    this.showEndPicker = false;
    this.apply = false;
  };

  public setPeriodObj() {

    const FORMAT_DATE_FOR_SERVER: string = 'YYYY-MM-DDTHH:mm:ss';
    const currentDate = moment().format( FORMAT_DATE_FOR_SERVER );
    this.periodObj = new Period();

    const getPeriodTime =  () => {
      const days = moment(this.periodObj.endDate).diff(moment(this.periodObj.startDate), 'days');
      if ( days < 7 ) {
        return ['Hourly' ]
      }
      if ( 7 <= days && days < 30 ) {
        return [ 'Daily', 'Weekly']
      }
      if ( 30 <= days && days < 356 ) {
        return [ 'Daily', 'Weekly', 'Monthly']
      }
      if ( 356 <= days) {
        return [ 'Weekly', 'Monthly']
      }
    };

    switch (this.selectedPeriod['id']) {
      case '0': // custom
        this.periodObj.startDate = moment(this.startDate).format( FORMAT_DATE_FOR_SERVER );
        this.periodObj.endDate = moment(this.endDate).format( FORMAT_DATE_FOR_SERVER );
        this.periodObj.time = getPeriodTime();
        break;
      case '1': // Today
        this.periodObj.startDate = moment().startOf('days').format( FORMAT_DATE_FOR_SERVER );
        this.periodObj.endDate = currentDate;
        this.periodObj.time = getPeriodTime();
        break;
      case '2': // Yesterday
        this.periodObj.startDate = moment().subtract(1, 'days').startOf('days').format( FORMAT_DATE_FOR_SERVER );
        this.periodObj.endDate = moment().endOf('days').subtract(1, 'days').format( FORMAT_DATE_FOR_SERVER );
        this.periodObj.time = getPeriodTime();
        break;
      case '3': // This week(Mon - Today)
        this.periodObj.startDate = moment().startOf('week').add( 1, 'days' ).format( FORMAT_DATE_FOR_SERVER );
        this.periodObj.endDate = currentDate;
        this.periodObj.time = getPeriodTime();
        break;
      case '4': // Last 30 days
        this.periodObj.startDate = moment( moment().subtract(30, 'days').format('YYYY-MM-DD')).format( FORMAT_DATE_FOR_SERVER );
        this.periodObj.endDate = currentDate;
        this.periodObj.time = getPeriodTime();
        break;
      case '5': // This year
        this.periodObj.startDate = moment().startOf('year').format( FORMAT_DATE_FOR_SERVER );
        this.periodObj.endDate = currentDate;
        this.periodObj.time = getPeriodTime();
        break;
      case '6': // All time
        this.periodObj.startDate = moment().subtract(8, 'years').format( FORMAT_DATE_FOR_SERVER );
        this.periodObj.endDate = currentDate;
        this.periodObj.time = getPeriodTime();
        break;
    }

    this.setPeriod.emit(this.periodObj)
  };

  public hideModal() {
    const period = this.period.find((item) => {
      return item.id === this.tmpPeriod.id
    });
    if ( !this.apply ) { this.selectedPeriod = period; }
    this.resetModal()
  };


  public setStartInputValue(e) {
    const d = moment(e.currentTarget.value);
    if (d.isValid() && moment(this.maxDate).isAfter(d) && moment('2000').isBefore(d) ) {
      this.startDate = d.toDate();
      this.minDate = d.toDate();
      return
    }
    e.currentTarget.value = moment(this.startDate).format('YYYY-MM-DD')
  };

  public setEndInputValue(e) {
    const  d = moment(e.currentTarget.value);
    if (d.isValid() && moment(this.maxDate).isAfter(d) && moment(this.minDate).isBefore(d) ) {
      this.endDate = d.toDate();
      return
    }
    e.currentTarget.value = moment(this.endDate).format('YYYY-MM-DD')
  };
}
