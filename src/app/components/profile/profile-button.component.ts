import { Component, OnInit } from "@angular/core";
import { AuthService } from '../../services/auth/auth.service'; 
import { ApiHttpService } from '../../services/api-http/api-http.service';

@Component({
	selector: "profile-dropdown",
	templateUrl: "./profile-button.component.html",
	styleUrls: ["./profile-button.component.scss"]
})

export class ProfileDropdownComponent implements OnInit {
	profile:any;
	constructor(private auth: AuthService, private api: ApiHttpService){}

	logout(){
		this.auth.logout();
	}

	ngOnInit(){
		this.api.get_ApiUsersV1Profile({}).subscribe((res)=>{
			this.profile = res.json();
		})
	}
}