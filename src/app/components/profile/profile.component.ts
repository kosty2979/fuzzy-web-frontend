import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Location } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { UserDataService } from "../../services/user-data/user-data.service";
import { ResolveUserService } from "../../services/user-data/resolve-user-data";


@Component({
	templateUrl: "./profile.component.html",
	styleUrls: ["./profile.component.scss"],
})

export class ProfileComponent {
	profile:any;
	newProfile:any;
	profileForm: FormGroup;
	formType:string = 'password'
	constructor(private location: Location, private userDataService: UserDataService, private fb: FormBuilder, private route: ActivatedRoute ){
		this.profile = this.route.snapshot.data.profile;
		this.profileForm = this.fb.group({
				personalInfo: this.fb.group({
					fullName: [this.profile.firstName],
					company: [this.profile.lastName],
				}),
				email: [this.profile.email],
				changePassword: this.fb.group({
					oldPassword: ['', [Validators.required]],
					matchPasswords: this.fb.group({
						newPassword: ['', [Validators.required]],
						confirmPassword: ['', [Validators.required]]
					}, { validator: this.equalValidator })
				})
			})
	}

	goBack(event){
		event.peventDefault();
		this.location.back();
	}

	changeType(event){
		event.target.checked ? this.formType = 'text' : this.formType = 'password';  
		
	}


	changeProfile(form){
		console.log(form);
	}

	saveInfo(form){
		console.log(form);
	}

	equalValidator({value}: FormGroup): {[key: string]: boolean} {
		let valid = value['newPassword'] === value['confirmPassword']; 
	    return valid ? null : {equal: true}
	}
}