import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'progressbar',
  templateUrl: './progressbar.component.html',
  styleUrls: ['./progressbar.component.scss']
})
export class ProgressbarComponent implements OnInit {

  @Input() type: string;
  @Input() value: number;
  classes: string[];

  constructor() {
    this.type = 'primary';
    this.classes = ['progress-bar'];
  }

  ngOnInit(): void {
    this.classes
      .push(`bg-${this.type}`);
  }

}
