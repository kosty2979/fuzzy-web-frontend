import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderConnectionComponent } from './provider-connection.component';

describe('ProviderConnectionComponent', () => {
  let component: ProviderConnectionComponent;
  let fixture: ComponentFixture<ProviderConnectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderConnectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
