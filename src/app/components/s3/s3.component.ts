import { Component, AfterViewInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { ApiHttpService } from 'app/services/api-http/api-http.service';
import { ImportTabComponent } from 'app/generic/components/imports/import-tab.component'

import 'rxjs/add/operator/mergeMap';


@Component({
  selector: 's3',
  templateUrl: './s3.component.html',
  styleUrls: ['./s3.component.scss']
})
export class S3Component extends ImportTabComponent implements AfterViewInit {
  keyLength: number;
  secretLength: number;

  constructor(private formBuilder: FormBuilder, private apiHttpService: ApiHttpService) {
    super();
  }

  constructorFunction(): void {
    this.keyLength = 20;
    this.secretLength = 40;
    this.submitted = false;
    // this.uploaded = true;
    this.validationClasses = {
      'url': {
        'formGroup': {
          'has-success': false,
          'has-danger': false
        },
        'formControl': {
          'form-control-success': false,
          'form-control-danger': false
        }
      },
      'key': {
        'formGroup': {
          'has-success': false,
          'has-danger': false
        },
        'formControl': {
          'form-control-success': false,
          'form-control-danger': false
        }
      },
      'secret': {
        'formGroup': {
          'has-success': false,
          'has-danger': false
        },
        'formControl': {
          'form-control-success': false,
          'form-control-danger': false
        }
      }
    };
    this.validationMessages = {
      'url': {
        'required': {
          'message': 'URL is required.'
        },
        'pattern': {
          'type': 'warning',
          'message': 'URL format must be <em>s3://bucket/name.ext</em>'
        }
      },
      'key': {
        'required': {
          'message': 'Key is required.'
        },
        'minlength': {
          'type': 'warning',
          'message': `Key must be ${this.keyLength} characters long.`
        },
        'maxlength': {
          'type': 'warning',
          'message': `Key must be ${this.keyLength} characters long.`
        }
      },
      'secret': {
        'required': {
          'message': 'Secret is required.'
        },
        'minlength': {
          'type': 'warning',
          'message': `Secret must be ${this.secretLength} characters long.`
        },
        'maxlength': {
          'type': 'warning',
          'message': `Secret must be ${this.secretLength} characters long.`
        }
      }
    };
    this.confirmData = {
      'title': 'Warning: Import deletes current sales data',
      'content': `Reimporting sales data will delete all existing sales data that was imported previously.`
    };
    this.confirmed = false;
    this.formErrors = [];
  }

  ngAfterViewInit() {
    this.apiHttpService.get_ApiImportsV1AuthS3Credentials({}).subscribe(
      res => {
        const res_json = res.json();
        if (res_json && 'aws_access_key_id' in res_json && 'aws_secret_access_key' in res_json) {
          this.form.get('key').setValue(res_json['aws_access_key_id']);
          this.form.get('secret').setValue(res_json['aws_secret_access_key']);
        } else {
          this.authorized = false;
        }
      }
    );
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      'url': [
        null,
        [
          Validators.required,
          Validators.pattern(/^(s3:\/\/)([\da-z\-]+)\/([\/\w \-]*)*\.([a-z\.]{2,6})$/)
          // (gs:\/\/)[a-z0-9_\-\.]+\/.+
          // Validators.pattern(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/)
        ]
      ],
      'key': [
        null,
        [
          Validators.required,
          Validators.minLength(this.keyLength),
          Validators.maxLength(this.keyLength)
        ]
      ],
      'secret': [
        null,
        [
          Validators.required,
          Validators.minLength(this.secretLength),
          Validators.maxLength(this.secretLength)
        ]
      ],
      'schedule': this.formBuilder.group({
        'reimport': null,
        'type': {
          value: null,
          disabled: true
        }
      })
    });

    this.form
      .valueChanges
      .subscribe(() => this.onValueChanged());
    this.form
      .get('schedule.reimport')
      .valueChanges
      .subscribe(reimport => this.onReimportValueChanged(reimport));

    // this.onValueChanged();
  }

  onValueChanged(): void {
    if (!this.form) {
      return;
    }
    const form = this.form;
    this.formErrors = [];

    for (const field in this.validationMessages) {
      if (this.validationMessages.hasOwnProperty(field)) {
        const control = form.get(field);
        // console.log(`Control Errors ${field}`, control.errors);

        if (!control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {

            if (control.errors.hasOwnProperty(key)) {
              // this.formErrors[field] += messages[key];
              const error = messages[key];
              this.formErrors.push(error);
              control.markAsTouched();
            }
          }
        }
      }
    }

    const urlTouched = form.get('url').touched,
          urlValid = form.get('url').valid,
          urlSuccess = urlTouched && urlValid,
          urlDanger = urlTouched && !urlValid,
          keyTouched = form.get('key').touched,
          keyValid = form.get('key').valid,
          keySuccess = keyTouched && keyValid,
          keyDanger = keyTouched && !keyValid,
          secretTouched = form.get('secret').touched,
          secretValid = form.get('secret').valid,
          secretSuccess = secretTouched && secretValid,
          secretDanger = secretTouched && !secretValid;

    this.validationClasses = {
      'url': {
        'formGroup': {
          'has-success': urlSuccess,
          'has-danger': urlDanger
        },
        'formControl': {
          'form-control-success': urlSuccess,
          'form-control-danger': urlDanger
        }
      },
      'key': {
        'formGroup': {
          'has-success': keySuccess,
          'has-danger': keyDanger
        },
        'formControl': {
          'form-control-success': keySuccess,
          'form-control-danger': keyDanger
        }
      },
      'secret': {
        'formGroup': {
          'has-success': secretSuccess,
          'has-danger': secretDanger
        },
        'formControl': {
          'form-control-success': secretSuccess,
          'form-control-danger': secretDanger
        }
      }
    };
  }

  onImportButtonClick(): void {
    this.onValueChanged();

    const url_input = this.form.get('url');
    const aws_secret_access_key = this.form.get('key');
    const aws_access_key_id = this.form.get('secret');

    if (url_input.valid && aws_secret_access_key.valid && aws_access_key_id.valid) {
      if (!this.confirmed) {
        this.confirm.onShow();
      } else {
        this.sendImportRequest(this.prepareCredentialsRequestParams(), this.prepareImportRequestParams());
      }
    }
  }

  private sendImportRequest(credentials, request_params) {
    this.apiHttpService.post_ApiImportsV1AuthS3({'apiImportsV1AuthS3': credentials}).mergeMap(() =>
      this.apiHttpService.post_ApiImportsV1S3({'apiImportsV1S3': request_params})
    ).subscribe(res =>  {if (res.ok) { this.submitted = true;}});
  }

  private prepareCredentialsRequestParams(): object {
    return {'aws_access_key_id': this.form.get('key').value,
      'aws_secret_access_key': this.form.get('secret').value};
  }

  onConfirm(event: boolean): void {
    if (event) {
      this.sendImportRequest(this.prepareCredentialsRequestParams(), this.prepareImportRequestParams());
      this.confirmed = event;
    }
  }
}
