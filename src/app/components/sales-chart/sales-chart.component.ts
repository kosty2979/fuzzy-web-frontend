import { Component, OnInit, Input, OnChanges, SimpleChange, ElementRef} from '@angular/core';
import { ReportService } from '../../services/report/report.service';
import { UserDataService } from '../../services/user-data/user-data.service';
import { LineChart } from '../../models/line-chart.model';
import { Period } from '../../models/period.model';
import { TrackerService } from '../../services/sp-tracker.service';
import * as moment from 'moment';

@Component({
  selector: 'sales-chart',
  templateUrl: './sales-chart.component.html',
  styleUrls: ['./sales-chart.component.scss']
})
export class SalesChartComponent implements OnInit, OnChanges {
  @Input() period: Period;
  public Chart: LineChart = new LineChart( [
    { text: 'Revenue', id: 'revenue', type: 'currency' },
    { text: 'Count', id: 'count', type: 'numeric' },
    { text: 'Avg.Value', id: 'avg_order_value', type: 'currency' }
  ]
  );
  public currentTime: Array<string> = [];
  public time: string ;
  public activeType: any;
  public load: boolean = false;

  constructor(
    private userData: UserDataService,
    private tracker: TrackerService,
    private reportService: ReportService
  ) {
    this.activeType = this.Chart.activeType;
    this.time = this.currentTime[0];
  }


  ngOnInit() {
    this.currentTime = this.period.time;
    this.setChartFormat();
  };

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes['period']) {
        this.currentTime = this.period.time;
        if( this.currentTime.indexOf(this.time) === -1) { this.time = this.currentTime[0];}
        this.getChartData();
      }
  };

  public getChartData() {
    // this.load = false;

    const data = {
      type: this.activeType.id,
      aggregateBy:  this.getAggrValue(),
      accountId: 'demo',
      startingFrom: this.period.startDate,
      endingWith: this.period.endDate
    };

     this.reportService.getSaleChartData(data).then((res: Response) => {
       let response = res.json();
       let labels: string[] = [];
       let dataSet: any[] = [];
       response['data'].map((item: { timestamp: string, value: string }) => {
           labels.push(item.timestamp);
           dataSet.push({x: item.timestamp, y: item.value});
         });
         this.Chart.labels = labels;
         this.Chart.data = [{ data: dataSet, label:  this.activeType.text }];
       if( this.Chart.labels.length !== 0 ) this.load = true;

       })
       .catch(( err ) => { console.log(err); });

  };




  public getAggrValue() {
    const baseAggr: { id: string, name: string}[] = [
      {id: 'hour', name: 'Hourly'},
      {id: 'day', name: 'Daily'},
      {id: 'week', name: 'Weekly'},
      {id: 'month', name: 'Monthly'}
    ];
    const aggr: { id: string, name: string } = baseAggr.find((item) => { return item.name === this.time; } );
    return aggr.id;
  };

  public setChartFormat() {
    this.Chart.options.scales.yAxes[0].ticks = {
      beginAtZero: true,
      callback: (label, index, labels) => {
        return this.activeType.type === 'numeric' ? (Math.round(label * 10) / 10) : ('$' + Math.round(label * 10) / 10);;
      }
    };
    this.Chart.options.scales.xAxes[0].ticks= {
      autoSkip : false,
        callback: function(value, index, values) {
        if( index === 0 || index === values.length-1 ) return value
      }
    };
    this.Chart.options.scales.xAxes[0].gridLines = {
      display : false,
    };

    this.Chart.options.tooltips = {
      backgroundColor: '#F0F0F0',
      bodyFontColor: '#111',
      titleFontColor: '#111',
      borderColor: 'rgba(0,0,0,1)',
      borderWidth: '1',
      callbacks: {
        title:  (tooltipItem, data) => {
          return moment(tooltipItem[0].xLabel).format('DD MMMM YYYY, h:mm a');
        },
        label:  (tooltipItems, data) => {
          return ' ' + data.datasets[tooltipItems.datasetIndex].label  + ': ' +
            (this.activeType.type === 'numeric' ? tooltipItems.yLabel : ('$' + tooltipItems.yLabel));
        }
      }
    };

    this.Chart.options.legend.display = false;
  };

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  };

  trackSelection(event, el: ElementRef, action, type){
    this.tracker.trackEvent(event, el, action, type);
  }

  public typeSelected(event, el, action, type) {

    this.trackSelection(event, el, action, type);

    if ( event.id !== this.activeType.id ) {
      this.activeType = this.Chart.type.find( (item) => {
        return item.id === event.id;
      });
      this.getChartData();
    }
  };

  public timeSelected(event, el, action, type) {

    this.trackSelection(event, el, action, type);

    if ( this.time !== event.text ) {
      this.time = event.text;
      this.getChartData();
    }
  };
}

