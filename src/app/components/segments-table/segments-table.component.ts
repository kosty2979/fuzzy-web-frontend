import {ChangeDetectionStrategy, Component, OnInit, ViewChild, ElementRef, HostListener, ChangeDetectorRef } from '@angular/core';
import {Period} from '../../models/period.model';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import { SegmentService } from '../../services/segment/segment.service';



@Component({
  selector: 'segments-table',
  templateUrl: './segments-table.component.html',
  styleUrls: ['./segments-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})


export class SegmentsTableComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('tableWrapper') tableWrapper: ElementRef;
  isDisabled:boolean = true;

  editing = {};
  data:any[];
  
  editingCols: Object = {};
  period: Period;
  selectsItem: any [] = [
    {
      value: 'sum',
      label: 'Sum'
    },
    {
      value: 'avg',
      label: 'Avg'
    }
  ];
  selectSumCurrCount: string;
  selectfbMatchCount: string;
  selectfbCpaBid: string;
  selectfbBudget: string;
  selectfbCtr: string;
  selectfbConvRate: string;
  selectfbCost: string;
  selectfbCpa: string;

  selectadwMatchCount: string;
  selectadwBidAdj: string;
  selectadwCtr: string;
  selectadwConvRate: string;
  selectadwCost: string;
  selectadwCpa: string;
  rows: any [];

  
  constructor(private segmentService:SegmentService, private cdr: ChangeDetectorRef) {
  };

  ngOnInit() {
    this.setSelect();
  };

  @HostListener('window:resize')
  onResize() {
    this.table.innerWidth = 0;
    setTimeout(() => {
      this.table.recalculate();
    }, 400)
  };


  public setSelect() {
    this.selectSumCurrCount = this.selectsItem[0].value;

    this.selectfbMatchCount = this.selectsItem[0].value;
    this.selectfbCpaBid = this.selectsItem[1].value;
    this.selectfbBudget = this.selectsItem[1].value;
    this.selectfbCtr = this.selectsItem[1].value;
    this.selectfbConvRate = this.selectsItem[1].value;
    this.selectfbCost = this.selectsItem[0].value;
    this.selectfbCpa = this.selectsItem[1].value;

    this.selectadwMatchCount = this.selectsItem[0].value;
    this.selectadwBidAdj = this.selectsItem[1].value;
    this.selectadwCtr = this.selectsItem[1].value;
    this.selectadwConvRate = this.selectsItem[1].value;
    this.selectadwCost = this.selectsItem[0].value;
    this.selectadwCpa = this.selectsItem[1].value;
  };

  public setRows() {
    this.segmentService.getRows(this.period).then((res)=>{
      this.data = res;
      this.rows = JSON.parse(JSON.stringify(this.data));
      this.cdr.markForCheck();
    });
  };

  public setPeriod(event) {
    this.period = event;
    this.setRows();
  };

  public activateEditMode(rowIndex, column){
    this.editing[rowIndex + column] = true;
  }

  // event
  public updateValue(event, cell, cellValue, row) {
    const value = +event.target.value.trim();
    this.editing[row.$$index + cell] = false;
    if (Number.isNaN(value) || value === 0 || this.rows[row.$$index][cell] == value) {
      return
    }
    this.rows[row.$$index][cell] = value;
    this.isDisabled = false
  };

  public updateColValue(event, column) {
    const value = +event.target.value.trim();
    this.editing[column] = false;
    if (Number.isNaN(value) || value === 0) {
      return
    }
    this.rows.forEach((item) => {
      item[column] = value;
    })
    this.isDisabled = false;
  };

  public getValue(prop, select) {
    if(!this.rows){
      return null
    }
    let sum = 0;
    this.rows.map((item) => {
      sum += +item[prop]
    });
    if (select === 'sum') { return Math.round(sum * 10) / 10; }
    const avg = sum / this.rows.length;
    return Math.round(avg * 10) / 10
  };

  public resetData() {
    this.rows = JSON.parse(JSON.stringify(this.data));
    this.isDisabled = true;
  };

  public getCellClass(index, name, value) {
    if (this.data[index][name] === value) {
      if (this.editingCols[index + name]) {
        delete this.editingCols[index + name]
      }
      return
    }
    this.editingCols[index + name] = true;
    return 'yellow'
  };
}
