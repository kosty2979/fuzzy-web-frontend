import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Response } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { ImportTabComponent } from 'app/generic/components/imports/import-tab.component'
import { ApiHttpService } from "app/services/api-http/api-http.service";


@Component({
  selector: 'shopify-import',
  templateUrl: './shopify-import.component.html'
})
export class ShopifyImportComponent extends ImportTabComponent {

  constructor(private formBuilder: FormBuilder, private apiHttpService: ApiHttpService) {
    super();
  }

  constructorFunction(): void {
    this.submitted = false;
    // this.uploaded = true;
    this.validationClasses = {
      'url': {
        'formGroup': {
          'has-success': false,

          'has-danger': false
        },
        'formControl': {
          'form-control-success': false,
          'form-control-danger': false
        }
      }
    };
    this.validationMessages = {
      'url': {
        'required': {
          'message': 'URL is required.'
        },
        'pattern': {
          'type': 'warning',
          'message': 'URL format must be <em>my-store-name.myshopify.com</em>'
        }
      }
    };
    this.confirmData = {
      title: 'Warning: Import deletes current sales data',
      content: `Reimporting sales data will delete all existing sales data that was imported previously.`
    };
    this.confirmed = false;
    this.formErrors = [];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.authStatusObservable.subscribe(auth_status_response => {
      const json_auth_response = auth_status_response.json();
      if (json_auth_response['status'] === 'authorized') {
        this.form.get('url').setValue(json_auth_response['url']+'.myshopify.com');
        this.authorized = true;
      }
    });
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      'type': 'shopify',
      'url': [
        null,
        [
          Validators.required,
          Validators.pattern(/^(https?:\/\/)?(([^.^_]+)\.)?myshopify\.com\/?$/)
        ]
      ],
      'isAuthorized': null,
    });
  }

  onValueChanged(): void {
    if (!this.form) {
      return;
    }
    const form = this.form;
    this.formErrors = [];

    for (const field in this.validationMessages) {
      if (this.validationMessages.hasOwnProperty(field)) {
        const control = form.get(field);

        if (!control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {

            if (control.errors.hasOwnProperty(key)) {
              // this.formErrors[field] += messages[key];
              const error = messages[key];
              this.formErrors.push(error);
              control.markAsTouched();
            }
          }
        }
      }
    }

    const urlTouched = form.get('url').touched,
          urlValid = form.get('url').valid,
          urlSuccess = urlTouched && urlValid,
          urlDanger = urlTouched && !urlValid;

    this.validationClasses = {
      'url': {
        'formGroup': {
          'has-success': urlSuccess,
          'has-danger': urlDanger
        },
        'formControl': {
          'form-control-success': urlSuccess,
          'form-control-danger': urlDanger
        }
      }
    };
  }

  onImportButtonClick(): void {
    this.onValueChanged();
    const url_input = this.form.get('url');
    if (url_input.valid) {
      if (!this.confirmed) {
        this.confirm.onShow();
      } else {this.sendHttpImportRequest(this.prepareImportRequestParams());}
    }
  }

  get authorizeShopifyObservable(): Observable<Response> {
    return this.apiHttpService.post_ApiImportsV1AuthShopify({'apiImportsV1AuthShopify': {'shop': this.form.get('url').value}});
  }

  get authStatusObservable(): Observable<Response> {
    return this.apiHttpService.get_ApiImportsV1AuthShopifyStatus({});
  }

  onAuthorizeButtonClick() {
    let auth_uri: string;
    this.authorizeShopifyObservable.subscribe(auth_uri_response => {
      const json_res = auth_uri_response.json();
      if (json_res && 'auth_uri' in json_res) {
        auth_uri = json_res['auth_uri'];
      }
    });

    const auth_subscription = Observable.timer(100, 500).subscribe(() => {
      if (auth_uri) {
        auth_subscription.unsubscribe();
        window.location.href = auth_uri;
      }
    });

  }

  prepareImportRequestParams(): object {
      return {'shop': this.form.get('url').value};
  }

  private sendHttpImportRequest(request_params) {
    this.apiHttpService.post_ApiImportsV1Shopify({'apiImportsV1Shopify': request_params }).subscribe(res => {
      if (res.ok) {
        this.submitted = true;
      }
    });
  }

  onConfirm(event: boolean): void {
    if (event) {
      this.sendHttpImportRequest(this.prepareImportRequestParams());
      this.confirmed = event;
    }
  }
}
