import { OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { ConfirmComponent } from 'app/components/confirm/confirm.component';


export abstract class ImportTabComponent implements OnInit {

  @ViewChild(ConfirmComponent) readonly confirm: ConfirmComponent;
  form: FormGroup;
  authorize: FormGroup;
  submitted: boolean;
  authorized: boolean;
  // uploaded: boolean;
  confirmData: object;
  confirmed: boolean;
  validationMessages: Object;
  validationClasses: any;
  formErrors: object[];

  protected constructor() {
    this.constructorFunction();
  }

  abstract constructorFunction(): void;
  abstract buildForm(): void;
  abstract onValueChanged(): void;
  abstract onConfirm(event: boolean): void

  ngOnInit(): void {
    this.buildForm();
  }

  onBlur() {
    this.onValueChanged();
  }

  onReimportValueChanged(reimport: boolean): void {
    if (reimport) {
      this.form
        .get('schedule.type')
        .enable();
    } else {
      this.form
        .get('schedule.type')
        .disable();
    }
  }

  prepareImportRequestParams(): object {
      const request_params = {'path': this.form.get('url').value, 'reimport': false, 'reimport_type': ''};
      const import_type = this.form.get('schedule.type');
      if (this.form.get('schedule.reimport').value && import_type.value) {
        request_params['reimport'] = true;
        request_params['reimport_type'] = import_type.value;
      }
      return request_params;
  }
}
