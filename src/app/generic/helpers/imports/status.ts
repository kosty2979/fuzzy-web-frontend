export class ImportStatusHelper {

  public setImportStatuses(scope) {
    scope.apiHttpService.get_ApiImportsV1Statuses({$queryParameters: {'limit': 1}}).subscribe(res => {
      const res_json = res.json().pop();
      scope.alerts = [];
      if ((res_json.length === 0) || (res_json['status'] !== 'IMPORTED' && res_json['status'] !== 'VERIFIED')) {
        scope.alerts.push({type: 'warning', message: '<strong>Warning!</strong> No sales data is imported yet. Go to <a href="app/sales">Sales</a>'});
      } else if (res_json['status'] === 'IMPORTED') {
        scope.alerts.push({type: 'warning', message: '<strong>Warning!</strong> Please verify imported sales data is correct. Once done click <a class="btn btn-primary" href="#" role="button">Approve</a>'});
      }
    });
  }
}
