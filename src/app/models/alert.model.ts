export class Alert {
  type: string;
  message: string;
  // bucket?: string;

  constructor({ type = 'danger', message = ''/*, bucket = null*/ } = { }) {
    this.type = type;
    this.message = message;
    // if (bucket) {
    //   this.bucket = bucket;
    // }
  }

  // get fullMessage() {
  //   return this.getFullMessage();
  // }

  // getFullMessage() {
  //   if (this.bucket) {
  //     return `${this.message} ${this.bucket}`;
  //   }
  //   return this.message;
  // }
}
