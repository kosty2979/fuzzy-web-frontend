export class LineChart {
  chartType: string;
  legend: boolean;
  type: {text: string, id: string, type: string}[];
  activeType: {text: string, id: string};
  labels: string[];
  data: any[];
  options: any;
  colors: any[];

  constructor( type ) {
    this.chartType = 'line';
    this.legend = true;
    this.type = type;
    this.activeType = this.type[1];
    this.labels = [];
    this.data = [];
    this.options = {
      tooltips: {
      },
      responsive: true,
      scales: {
        yAxes: [
          {
            ticks: {

              },
            scaleLabel: {
            }
          }
        ],
        xAxes: [{
          type: 'time',
          time: {
            displayFormats: {
              'millisecond': 'HH:mm',
              'second': 'HH:mm',
              'minute': 'HH:mm',
              'hour': 'DD HH:mm',
              'day': 'MMM DD ',
              'week': 'MMM DD',
              'month': 'YYYY MMM DD',
              'quarter': 'YYYY MMM DD',
              'year': 'YYYY MMM DD',
            }
          }
        }],
      },
      legend: {
      }
    };
    this.colors = [
      { // grey
        backgroundColor: 'rgba(136,136,136,0.2)',
        borderColor: 'rgba(136,136,136,1)',
        pointBackgroundColor: 'rgba(136,136,136,1)',
        pointBorderColor: 'gba(136,136,136,0.2)',
        pointHoverBackgroundColor: '#888888',
        pointHoverBorderColor: 'rgba(136,136,136,0.8)'
      },
      { // light grey
        backgroundColor: 'rgba(211,211,211,0.2)',
        borderColor: 'rgba(211,211,211,1)',
        pointBackgroundColor: 'rgba(211,211,211,1)',
        pointBorderColor: 'rgba(211,211,211,0.2)',
        pointHoverBackgroundColor: '#D3D3D3',
        pointHoverBorderColor: 'rgba(211,211,211,0.8)'
      },
      { // dark grey
        backgroundColor: 'rgba(88,88,88,0.2)',
        borderColor: 'rgba(88,88,88,1)',
        pointBackgroundColor: 'rgba(88,88,88,1)',
        pointBorderColor: 'rgba(88,88,88,0.2)',
        pointHoverBackgroundColor: '#585858',
        pointHoverBorderColor: 'rgba(88,88,88,0.8)'
      }
    ];
  };
}

