export class Period {
  time: string[];
  endDate: string;
  startDate: string;

  constructor() {
    this.time = [];
    this.endDate = '';
    this.startDate = '';
  }
}
