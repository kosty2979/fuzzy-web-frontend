export class TableRow {
    constructor(
      private name: string,
      private recency: string,
      private frequency: string,
      private monetary: string,
      private currentCount: number,
      private fbPublished: boolean,
      private fbMatchCount: number,
      private fbCpaBid: number,
      private fbBudget: number,
      private fa_ctr: number,
      private fa_conversions: number,
      private fa_cost: number,
      private fa_cpa: number,
      private adwPublished: boolean,
      private adwMatchCount: number,
      private adwBidAdj: number,
      private ad_ctr: number,
      private ad_conversions: number,
      private ad_cost: number,
      private ad_cpa: number,
      ){}
}