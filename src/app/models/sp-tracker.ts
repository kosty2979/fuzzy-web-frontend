export class Contexts{
	constructor(
		private schema: string,
		private data: ContextData | ErrContextData
		){}
}

export class ContextData{
	constructor( 
		private action: string, 
		private type: string, 
		private url: string,
		private element: string,
		private elementId: string,
		private text: string,
		){}
}

export class ErrContextData{
	constructor( 
		private type: string, 
		private url: string,
		private apiUrl?: string,
		private message?: string,
		private stack?: string,
	){}
}