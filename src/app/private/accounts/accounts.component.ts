import { Component, OnInit, ViewChild } from '@angular/core';

import { Logger } from 'angular2-logger/core';

import { FacebookAccountComponent } from '../../components/facebook-account/facebook-account.component';
import { AdwordsAccountComponent } from '../../components/adwords-account/adwords-account.component';
import { TwitterAccountComponent } from '../../components/twitter-account/twitter-account.component';

import { AccountsService } from '../../services/accounts/accounts.service';

import { environment } from '../../../environments/environment';


const ASSETS_BASE_URL = environment.assetsBaseUrl;


@Component({
  selector: 'accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  facebookConnection: any = { };
  adwordsConnection: any = { };
  twitterConnection: any = { };
  facebookImagePath: string;
  adwordsImagePath: string;
  twitterImagePath: string;

  @ViewChild(FacebookAccountComponent) readonly facebookAccountModal: FacebookAccountComponent;
  @ViewChild(AdwordsAccountComponent) readonly adwordsAccountModal: AdwordsAccountComponent;
  @ViewChild(TwitterAccountComponent) readonly twitterAccountModal: TwitterAccountComponent;

  constructor(private accountsService: AccountsService, private logger: Logger) {
    this.facebookImagePath = `${ASSETS_BASE_URL}/images/facebook.png`;
    this.adwordsImagePath = `${ASSETS_BASE_URL}/images/adwords`;
    this.twitterImagePath = `${ASSETS_BASE_URL}/images/twitter.jpg`;
  }

  ngOnInit() {
    this.getUserConnections();
  }

  showFacebookAccountModal() {
    this.facebookAccountModal.showModal();
  }

  showAdwordsAccountModal() {
    this.adwordsAccountModal.showModal();
  }

  showTwitterAccountModal() {
    this.twitterAccountModal.showModal();
  }

  getUserConnections() {
    this.accountsService
      .onGetUserConnections()
      .subscribe(res => {
        const response = res.json();
        this.logger.log('this.accountsService.onGetUserConnections().subscribe()', response);

        // tslint:disable-next-line:forin
        for (const connection in response) {
          if (response[connection]['provider'] === 'FB') {
            this.facebookConnection.id = response[connection].id;
            this.facebookConnection.email = response[connection].email;
            this.facebookConnection.accountId = response[connection].account_id;
            this.facebookConnection.accountName = response[connection].account_name;
            this.facebookConnection.isConnected = true;
          }

          if (response[connection]['provider'] === 'ADWORDS') {
            this.adwordsConnection.id = response[connection].id;
            this.adwordsConnection.email = response[connection].email;
            this.adwordsConnection.accountId = response[connection].account_id;
            this.adwordsConnection.accountName = response[connection].account_name;
            this.adwordsConnection.isConnected = true;
          }

          if (response[connection]['provider'] === 'TWITTER') {
            this.twitterConnection.id = response[connection].id;
            this.twitterConnection.email = response[connection].email;
            this.twitterConnection.accountId = response[connection].account_id;
            this.twitterConnection.accountName = response[connection].account_name;
            this.twitterConnection.isConnected = true;
          }
        }

        if (!this.facebookConnection.isConnected) {
          this.facebookConnection.isConnected = false;
        }
        if (!this.adwordsConnection.isConnected) {
          this.adwordsConnection.isConnected = false;
        }
        if (!this.twitterConnection.isConnected) {
          this.twitterConnection.isConnected = false;
        }
      });
  }

}
