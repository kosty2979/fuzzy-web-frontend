import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalesComponent } from './sales/sales.component';
import { AccountsComponent } from './accounts/accounts.component';
import { ReportsComponent } from './reports/reports.component';
import {SegmentsComponent} from "./segment/segments.component";
import { ProfileComponent } from "../components/profile/profile.component";

import { ResolveUserService } from '../services/user-data/resolve-user-data';



const routes: Routes = [
  {
    path: '',
    // data: {
    //   title: 'Private'
    // },
    children: [
      {
        path: '',
        redirectTo: 'sales',
        pathMatch: 'full'
      },
      {
        path: 'sales',
        component: SalesComponent,
        data: {
          title: 'Sales'
        }
      },
      {
        path: 'connections',
        component: AccountsComponent,
        data: {
          title: 'Connections'
        }
      },
      { 
        path: 'profile', 
        component: ProfileComponent,
        resolve: {
          profile: ResolveUserService
        } 
      },
      {
        path: 'reports',
        component: ReportsComponent,
        data: {
          title: 'Reports'
        }
      },
      {
        path: 'segments',
        component: SegmentsComponent,
        data: {
          title: 'Segments'
        }
      },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ResolveUserService]
})
export class PrivateRoutingModule {}
