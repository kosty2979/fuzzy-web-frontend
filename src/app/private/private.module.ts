import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { ToasterModule } from 'angular2-toaster/angular2-toaster';

import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { SelectModule } from 'ng2-select/ng2-select';
import { DatepickerModule } from 'ngx-bootstrap/datepicker';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import {FacebookModule} from 'ngx-facebook';


import {PrivateRoutingModule} from './private-routing.module';

import { SalesComponent } from './sales/sales.component';
import { FileImportComponent } from '../components/file-import/file-import.component';
import { S3Component } from '../components/s3/s3.component';
import { GoogleStorageComponent } from '../components/google-storage/google-storage.component';
import { FileFormatComponent } from '../components/file-format/file-format.component';
import { FacebookAccountComponent } from '../components/facebook-account/facebook-account.component';
import { AdwordsAccountComponent } from '../components/adwords-account/adwords-account.component';
import { TwitterAccountComponent } from '../components/twitter-account/twitter-account.component';
import { AccountsComponent } from './accounts/accounts.component';
import { ReportsComponent } from './reports/reports.component';
import { ProviderConnectionComponent } from '../components/provider-connection/provider-connection.component';
import { SalesChartComponent } from '../components/sales-chart/sales-chart.component';
import { AdvertisementChartComponent } from '../components/advertisement-chart/advertisement-chart.component';
import { PeriodSelectComponent } from '../components/period-select/periodSelect';
import { SegmentsComponent } from './segment/segments.component';
import { ProfileComponent } from "../components/profile/profile.component";
import { SegmentsTableComponent } from '../components/segments-table/segments-table.component';

import { ReportService } from '../services/report/report.service';
import { SegmentService } from '../services/segment/segment.service';
import { FormService } from '../services/form/form.service';
import { AccountsService } from '../services/accounts/accounts.service';
import { UserDataService } from '../services/user-data/user-data.service';


import { AutofocusDirective } from 'app/directives/autofocus-directive/autofocus.directive';
import { ShopifyImportComponent } from "app/components/shopify-import/shopify-import.component";

@NgModule({
  imports: [
    PrivateRoutingModule,
    SharedModule,
    ChartsModule,
    ReactiveFormsModule,
    FacebookModule,
    FileUploadModule,
    ToasterModule,
    FormsModule,
    TabsModule,
    SelectModule,
    TooltipModule.forRoot(),
    DatepickerModule.forRoot(),
    MultiselectDropdownModule,
    NgxDatatableModule
  ],
  declarations: [
    SalesComponent,
    FileImportComponent,
    S3Component,
    ShopifyImportComponent,
    GoogleStorageComponent,
    FileFormatComponent,
    FacebookAccountComponent,
    AdwordsAccountComponent,
    TwitterAccountComponent,
    AccountsComponent,
    ReportsComponent,
    ProviderConnectionComponent,
    PeriodSelectComponent,
    SalesChartComponent,
    AdvertisementChartComponent,
    SegmentsComponent,
    ProfileComponent,
    SegmentsTableComponent,
    AutofocusDirective,
  ],
  providers: [
    FormService,
    AccountsService,
    ReportService,
    UserDataService,
    SegmentService,
  ]
})
export class PrivateModule {
}
