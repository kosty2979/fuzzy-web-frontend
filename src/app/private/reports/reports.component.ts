import { Component, OnInit } from '@angular/core';
import { Period } from 'app/models/period.model';
import { ImportStatusHelper } from 'app/generic/helpers/imports/status'
import { ApiHttpService } from 'app/services/api-http/api-http.service';
import { TrackerService } from '../../services/sp-tracker.service';

@Component({
  selector: 'reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})

export class ReportsComponent extends ImportStatusHelper implements OnInit {
  tabs: object[];
  alerts: object[];
  periodObj: Period;
  typeEl:string;


  constructor(private apiHttpService: ApiHttpService, private tracker: TrackerService) {
    super();
  }
  
  trackSelection(event, action, type){
    this.tracker.trackEvent(event, event.elementRef, action, type);
  }

  ngOnInit() {
    this.tabs = [
      {heading: 'Sales', id: 'tab-sales'},
      {heading: 'Advertisement Perfomance', id: 'tab-advertisement'}
    ];
    this.setImportStatuses(this);
  };
}
