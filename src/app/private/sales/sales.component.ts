import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { ApiHttpService } from 'app/services/api-http/api-http.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import { Subscription } from 'rxjs/Subscription';
import {Router} from "@angular/router";
import {ToasterConfig, ToasterService, Toast} from "angular2-toaster";

const STATUS_MAPPING = {
  'INVALID': {type: 'danger', message: "<strong>Error!</strong> Your file couldn't be imported. Please verify if it is in correct format as per example below"},
  'IMPORTED': {type: 'success', message: "<strong>Well done!</strong> Your sales data is imported. Please verify it in reports, if it's correct. Go to <a href='/app/reports'>Reports</a> "},
  'UPLOADING': {type: 'warning', message: '<strong>Wait!</strong> Your sales data importing right now'}
};
const NO_IMPORTS_MESSAGE = {type: 'warning', message: '<strong>Warning!</strong> No sales data is imported yet'};


@Component({
  selector: 'sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements AfterViewInit, OnDestroy {

  tabs: object[];
  alerts: object[];
  notifications_subscription: Subscription;
  toasterConfig: ToasterConfig =
    new ToasterConfig({
      tapToDismiss: false,
      timeout: 10000,
      positionClass: 'toast-top-left'
    });
  authorizationMessage: Toast;

  constructor(private apiHttpService: ApiHttpService, private router: Router,
              private toasterService: ToasterService) {
    this.tabs = [
      { heading: 'File Import', name: 'file', active: true },
      { heading: 'S3 Bucket', name: 's3', active: false },
      { heading: 'Google Storage Bucket', name: 'gs', active: false },
      { heading: 'Shopify', name: 'shopify', active: false }
    ];

    const parsedUrl = this.router.parseUrl(this.router.url);
    const [urlFragment, queryParams] = [parsedUrl.fragment, parsedUrl.queryParams];

    if (urlFragment) {
      for (const tab of this.tabs) {
        tab['active'] = tab['name'] === urlFragment;
      }
    }

    if (queryParams.hasOwnProperty('auth') && queryParams.hasOwnProperty('status')) {
      const importType = queryParams['auth'];
      if (queryParams['status'] === 'authorized') {
        this.authorizationMessage = {type: 'success', title: `${importType} authorization`, body: 'It works!'};
      } else {
        this.authorizationMessage = {type: 'error', title: `${importType} authorization`, body: queryParams['status'].replace('+', " ")};
      }
    }
  }

  ngAfterViewInit() {
    if (this.authorizationMessage) {
      // console.log(this.authorizationMessage);
      this.toasterService.popAsync(this.authorizationMessage).subscribe();
    }
    this.getImportStatuses();
    this.notifications_subscription = Observable.interval(15000).subscribe(
      () => this.getImportStatuses()
    );
  }

  ngOnDestroy() {
    this.notifications_subscription.unsubscribe();
  }

  getImportStatuses() {
    return this.apiHttpService.get_ApiImportsV1Statuses({$queryParameters: {'limit': 1}})
      .subscribe(response => {
        this.alerts = [];
        const response_array = response.json();
        // if (!response_array['status']) {
        //   this.notifications_subscription.unsubscribe();
        // }
        if (response_array.length > 0) {
          for (const status of response_array) {
            this.alerts.push(STATUS_MAPPING[status['status']]);
            if (status['status'] !== 'UPLOADING') {
              this.notifications_subscription.unsubscribe();
            }
          }
        } else {
          this.alerts.push(NO_IMPORTS_MESSAGE);
          this.notifications_subscription.unsubscribe();
        }
    });
  }
}
