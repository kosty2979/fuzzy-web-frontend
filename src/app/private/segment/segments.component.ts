import { Component, OnInit } from '@angular/core';
import { Period } from 'app/models/period.model';
import { ImportStatusHelper } from 'app/generic/helpers/imports/status';
import { ApiHttpService } from 'app/services/api-http/api-http.service';



@Component({
  selector: 'segments',
  templateUrl: './segments.component.html',
  styleUrls: ['./segments.component.scss']
})

export class SegmentsComponent extends ImportStatusHelper implements OnInit {

  alerts: object[];
  periodObj: Period;


  constructor(private apiHttpService: ApiHttpService) {
    super();
  };

  ngOnInit() {
    this.setImportStatuses(this);
  };
}
