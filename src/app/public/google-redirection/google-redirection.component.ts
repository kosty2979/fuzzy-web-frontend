import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Observable } from 'rxjs/Observable';
import { CookieService } from '../../services/cookie/cookie.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-google-redirection',
  templateUrl: './google-redirection.component.html',
  styleUrls: ['./google-redirection.component.scss']
})
export class GoogleRedirectionComponent implements OnInit {
  tokens: {
    'access_token': string,
    'refresh_token': string,
    'expires_in': string
  };
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
    const tree = this.router.parseUrl(this.router.url);
    const tokensTree = tree.queryParams;
    this.tokens = {
      'access_token': tokensTree['access_token'],
      'refresh_token': tokensTree['refresh_token'],
      'expires_in': tokensTree['expires_in']
    }
    this.saveToken(this.tokens).subscribe(
      res => res === true ? this.router.navigateByUrl('sales') : alert('Token wasn\'t saved. ' +
        'Try again, or if it still isn\'t working check for allowing to use cookie on your browser!'),
      err => console.log('ERROR: ' + err)
      )
  }

  private saveToken(tokens: {}): Observable<boolean> {
    if ('access_token' in tokens && 'refresh_token' in tokens && 'expires_in' in tokens) {
      CookieService.set(tokens['refresh_token'], 'refresh_token');
      CookieService.set(tokens['access_token'], 'access_token');
      CookieService.set(true, 'is_token_valid', tokens['expires_in']);
      this.auth.isLoggedIn.next(true);
    }
    return Observable
      .of(this.auth.isLoggedIn.getValue());
  }

}
