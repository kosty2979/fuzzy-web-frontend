import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from '../../../environments/environment.staging';

import { AuthService } from '../../services/auth/auth.service';
import { TrackerService } from '../../services/sp-tracker.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';



const PREFILL = environment.prefill;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  @ViewChild('loginBtn') loginBtn:ElementRef;

  assetsUrl = environment.assetsBaseUrl;

  states = {
    processing: false,
    error: false
  }
  constructor(private authService: AuthService, private router: Router, private tracker: TrackerService) {};

  ngOnInit() { }


  onLogin(username: string, password: string ) {
    this.states.error = false;
    // localStorage.setItem('fuzzy-lab-user', username);
    this.authService.isLoggedIn.subscribe();
    this.states.processing = true;
    this.authService
      .login(username, password)
      .subscribe(val => {
        this.tracker.trackEvent( '', this.loginBtn, 'login', 'auth');
        this.router.navigate([this.authService.redirectUrl])
      .then(() => {
        this.states.processing = false;
      })
      },
      err =>{
        this.states.processing = false;
        this.states.error = true;
        this.tracker.trackEvent( '', this.loginBtn, 'login', 'validation', err);
        return Observable.throw(err);
      });
  }
  onGoogle() {
    window.location.href = 'https://services.fuzzylabsresearch.com/auth/oauth2/login/google-oauth2/';
  }

}
