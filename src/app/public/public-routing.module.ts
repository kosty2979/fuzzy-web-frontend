import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { GoogleRedirectionComponent } from './google-redirection/google-redirection.component';
import { RegistrationComponent } from './registration/registration.component';
import { ResetComponent } from './reset/reset.component';
import { ResetConfirmComponent } from './reset-confirm/reset-confirm.component';
const routes: Routes = [
  {
    path: '',
    // data: {
    //   title: 'Public'
    // },
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent,
        data: {
          title: 'Login'
        }
      },
      {
        path: 'register',
        component: RegistrationComponent
      },
      {
        path: 'auth/redirect/google',
        component: GoogleRedirectionComponent
      },
      {
        path: 'reset',
        component: ResetComponent
      },
      {
        path: 'reset/confirm',
        component: ResetConfirmComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
