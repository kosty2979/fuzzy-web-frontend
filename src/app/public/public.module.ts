import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';

import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { GoogleRedirectionComponent } from './google-redirection/google-redirection.component';
import { ResetComponent } from './reset/reset.component';
import { ResetConfirmComponent } from './reset-confirm/reset-confirm.component';

@NgModule({
  imports: [
    CommonModule,
    PublicRoutingModule
  ],
  declarations: [
    LoginComponent,
    RegistrationComponent,
    GoogleRedirectionComponent,
    ResetComponent,
    ResetConfirmComponent
  ]
})
export class PublicModule { }
