import { Component, OnInit, ViewChild} from '@angular/core';
import { ApiHttpService} from '../../services/api-http/api-http.service';
import 'rxjs/Rx';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
 export class RegistrationComponent implements OnInit {
  @ViewChild('password') password: any;
  @ViewChild('repeat_password') repeat_password: any;
  @ViewChild('email') email: any;
  @ViewChild('first_name') first_name: any;
  @ViewChild('last_name') last_name: any;
  @ViewChild('company') company: any;
  @ViewChild('account_name') account_name: any;

  fields: {};

  confirm = {
    status: false,
    message: 'Your account is being created. Please check your email in few minutes for login details.'
  };

  constructor(private apiHttp: ApiHttpService) { }

  ngOnInit() {
    this.fields = {
      'password' : this.password,
      'repeat_password' : this.repeat_password,
      'email' : this.email,
      'first_name' : this.first_name,
      'last_name' : this.last_name,
      'company' : this.company,
      'account_name': this.account_name
    };
  }

  onCreate() {
    if (this.password.nativeElement.value !== this.repeat_password.nativeElement.value) {
      this.password.nativeElement.value = '';
      this.repeat_password.nativeElement.value = '';
      this.password.nativeElement.placeholder = 'Your repeat password field does not match your password';
      this.password.nativeElement.style.borderColor = 'red';
      return;
    }
    this.apiHttp.post_AuthV1Registration({
      'authV1Registration': {
        'email': this.email.nativeElement.value,
        'password': this.password.nativeElement.value,
        'first_name': this.first_name.nativeElement.value,
        'last_name': this.last_name.nativeElement.value,
        'account_name': this.account_name.nativeElement.value,
        'company': this.company.nativeElement.value
      }}).subscribe(
        (res) => {
          for (let key in this.fields) {
            this.fields[key].nativeElement.value = ''
          }
          this.confirm.status = true;
        },
        (err) => {
          for (let field in this.fields) {
            this.fields[field].nativeElement.style.borderColor = '#E4E6EB';
          }
          const e = JSON.parse(err);
          for (let key in e) {
              for (let field in this.fields) {
                if (field.valueOf() === key) {
                  this.fields[field].nativeElement.value = '';
                  this.fields[field].nativeElement.placeholder = e[key];
                  this.fields[field].nativeElement.style.borderColor = 'red';
                }
              }
          }
        }
    )
  }
}


