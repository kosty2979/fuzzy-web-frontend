import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { ApiHttpService} from '../../services/api-http/api-http.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset-confirm.component.html',
  styleUrls: ['./reset-confirm.component.scss']
})
export class ResetConfirmComponent implements OnInit {

  statuses = {
    error: {
      status: false,
      message: ''
    },
    confirm: {
      status: false,
      message: 'Password was reseted successfully!'
    }
  }

  constructor(private api: ApiHttpService, private router: Router) { }

  ngOnInit() {
  }

  onCreate(password: string, repeat_password: string) {
    if (password !== repeat_password) {
      this.statuses.error.status = true;
      this.statuses.error.message = 'Passwords don\'t match!';
      return;
    }
    const tree = this.router.parseUrl(this.router.url);
    const tokensTree = tree.queryParams;
    const tokens = {
      'new_password1': password,
      'new_password2': repeat_password,
      'uuid': tokensTree['reset_uuid']
    };
    this.api.post_ApiAuthV1PasswordResetConfirm({
      'apiAuthV1PasswordResetConfirm': tokens
    }).subscribe(
      res => {
        this.statuses.error.status = false;
        this.statuses.confirm.status = true;
      },
      err => {
        this.statuses.error.status = true;
        this.statuses.error.message = err;
      }
    );
  }

}
