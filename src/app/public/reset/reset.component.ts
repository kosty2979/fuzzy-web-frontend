import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { ApiHttpService} from '../../services/api-http/api-http.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {

  statuses = {
    error: {
      message: '',
      status: false
    },
    complete: {
      message: 'Check your email! We send you a message',
      status: false
    }
  };

  constructor(private router: Router, private api: ApiHttpService) { }

  ngOnInit() {
  }

  onReset(email: string) {
    this.statuses.error.status = false;
    this.statuses.complete.status = false;
    this.api.post_ApiAuthV1PasswordReset({
      'apiAuthV1PasswordReset': {
        'email': email
      }
    }).subscribe(
      res => {
        this.statuses.complete.status = true
      },
      err => {
        const e = JSON.parse(err);
        this.statuses.error.message = e['email'];
        this.statuses.error.status = true
      }
    );
  }

}
