import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ApiHttpService } from '../api-http/api-http.service';

import { environment } from '../../../environments/environment';


const FACEBOOK_APP_ID = environment.facebookAppId;


@Injectable()
export class AccountsService {

  constructor(private apiHttpService: ApiHttpService) { }

  onGetUserConnections(): Observable<Response> {
    return this.apiHttpService.get_ApiConnectionsV1User({});
  }

  onConnectToFacebook(email: string, accountId: string, accountName, accessToken: string):
    Observable<Response> {
    const params = {
      'email': email,
      'account_id': accountId,
      'account_name': accountName,
      'token': accessToken,
      'provider': '',
      'status': '',
      'user': '',
      'client_id': FACEBOOK_APP_ID
    };

    return this.apiHttpService.post_ApiConnectionsV1Fb({
      'apiConnectionsV1Fb': params
    });
  }

  onDisconnectFromFacebook(id: string): Observable<Response> {
    return this.apiHttpService.delete_ApiConnectionsV1FbId({
      'id': id
    });
  }

  onTestFacebookConnection(id: string): Observable<Response> {
    return this.apiHttpService.get_ApiConnectionsV1FbVarTest({
      'var': id
    });
  }

  onGetCodeForAdwords(): Observable<Response> {
    return this.apiHttpService.get_ApiConnectionsV1AdwordsAuth({});
  }

  onSendCodeToAdwords(code: string): Observable<Response> {
    return this.apiHttpService.post_ApiConnectionsV1Adwords({
      $queryParameters: {'code': code}
    });
  }

  onConnectToAdwords(email: string, accountId: string): Observable<Response> {
    const params = {
      'email': email,
      'account_id': accountId,
      'provider': '',
      'status': '',
      'token': '',
    };

    return this.apiHttpService.post_ApiConnectionsV1Adwords({
      'apiConnectionsV1Adwords': params
    });
  }

  onDisconnectFromAdwords(id: string): Observable<Response> {
    return this.apiHttpService.delete_ApiConnectionsV1AdwordsId({
      'id': id
    });
  }

  onTestAdwordsConnection(id: string): Observable<Response> {
    return this.apiHttpService.get_ApiConnectionsV1AdwordsVarTest({
      'var': id
    });
  }

}
