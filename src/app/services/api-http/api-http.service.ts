import {
    Injectable,
    Injector
} from '@angular/core';
import {
    Http,
    RequestOptions,
    RequestMethod,
    Response
} from '@angular/http';
import {
    Observable
} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {
    CookieService
} from '../cookie/cookie.service';
import {
    AuthService
} from '../auth/auth.service';
import {
    Router
} from '@angular/router';

import {
    environment
} from '../../../environments/environment';

const PUBLIC_URLS_LIST = ['/auth/v1/registration', '/auth/v1/get_token', '/auth/v1/get_token/oauth', '/auth/v1/password/reset/confirm', '/auth/v1/password/reset', '/auth/v1/redirect/gs', '/auth/v1/redirect/shopify', '/auth/v1/password/reset', '/auth/v1/password/reset/confirm'];

@Injectable()
export class ApiHttpService {
    private authService: AuthService;

    constructor(private http: Http, private router: Router, private injector: Injector) {
        setTimeout(() => this.authService = this.injector.get(AuthService)); // timeout for circular dependencies
    }

    execute(method: string, url: string, path: string, params ? : {
        [key: string]: any | any[]
    }, auth: boolean = true): Observable < Response > {
        const requestConfig = {
            method: RequestMethod[method.charAt(0).toUpperCase() + method.slice(1).toLowerCase()],
            body: {},
            params: {}
        };
        if (requestConfig.method === RequestMethod['Get']) {
            requestConfig.params = params;
        } else {
            requestConfig.body = params
        }
        let fullPath: string;

        if (auth) {
            const access_token = CookieService.get('access_token');
            requestConfig.params = Object.assign(requestConfig.params, {
                'access_token': access_token
            });
        }
        if (url.startsWith('http')) {
            fullPath = `${url}${path}`;
        } else {
            if (auth) {
                fullPath = `${environment.apiUrl}/api/${url}/${path}`;
            } else {
                fullPath = `${environment.publicApiUrl}/${url}/${path}`;
            }
        }
        const options = new RequestOptions(requestConfig);
        console.log(fullPath);
        return this.http
            .request(fullPath, options)
            .map(this.extractData)
            .catch(error => this.handleError(error));
    }

    private extractData(res: Response) {
        // console.log('extractData()', res);
        // const body = res.json();
        // console.log('body', body);
        return res;
    }

    private handleError(error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        if (errMsg.toLowerCase().includes('token')) {
            this.authService.redirectUrl = this.router.url;
            this.router.navigate(['/login']).then();
            CookieService.clear();
        }
        return Observable.throw(errMsg);
    }

    submitTransaction(parameters: {
        'transactionRequest' ? : SalesApiTransactionRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/sales/v1/submit/transaction';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('transactionRequest' in parameters) {
            params = parameters['transactionRequest'];
        }
        const name_lower = 'submitTransaction'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    submitTransactionFlattened(parameters: {
        'transactionRequest' ? : SalesApiTransactionFlattenedRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/sales/v1/submit/transaction_flattened';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('transactionRequest' in parameters) {
            params = parameters['transactionRequest'];
        }
        const name_lower = 'submitTransactionFlattened'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    checkTransaction(parameters: {
        'transactionRequest' ? : SalesApiTransactionRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/sales/v1/check/transaction';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('transactionRequest' in parameters) {
            params = parameters['transactionRequest'];
        }
        const name_lower = 'checkTransaction'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    checkTransactionFlattened(parameters: {
        'transactionRequest' ? : SalesApiTransactionFlattenedRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/sales/v1/check/transaction_flattened';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('transactionRequest' in parameters) {
            params = parameters['transactionRequest'];
        }
        const name_lower = 'checkTransactionFlattened'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    querySales(parameters: {
        'request' ? : ReportingApiSalesRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/reports/v1/query/sales';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('request' in parameters) {
            params = parameters['request'];
        }
        const name_lower = 'querySales'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    querySegments(parameters: {
        'request' ? : ReportingApiSegmentsRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/reports/v1/query/segments';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('request' in parameters) {
            params = parameters['request'];
        }
        const name_lower = 'querySegments'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    querySalesBatch(parameters: {
        'request' ? : ReportingApiBatchSalesRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/reports/v1/query/sales/batch';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('request' in parameters) {
            params = parameters['request'];
        }
        const name_lower = 'querySalesBatch'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    querySegmentsBatch(parameters: {
        'request' ? : ReportingApiBatchSegmentsRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/reports/v1/query/segments/batch';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('request' in parameters) {
            params = parameters['request'];
        }
        const name_lower = 'querySegmentsBatch'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    updateSales(parameters: {
        'request' ? : ReportingApiUpdateRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/reports/v1/update/sales';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('request' in parameters) {
            params = parameters['request'];
        }
        const name_lower = 'updateSales'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    querySegmentsDetail(parameters: {
        'request' ? : ReportingApiSegmentsDetailRequest,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/reports/v1/query/segments/detail';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('request' in parameters) {
            params = parameters['request'];
        }
        const name_lower = 'querySegmentsDetail'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    post_ApiAuthV1PasswordResetConfirm(parameters: {
        'apiAuthV1PasswordResetConfirm' ? : ApiAuthV1PasswordResetConfirm,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/auth/v1/password/reset/confirm';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiAuthV1PasswordResetConfirm' in parameters) {
            params = parameters['apiAuthV1PasswordResetConfirm'];
        }
        const name_lower = 'post_ApiAuthV1PasswordResetConfirm'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    post_ApiAuthV1PasswordReset(parameters: {
        'apiAuthV1PasswordReset' ? : ApiAuthV1PasswordReset,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/auth/v1/password/reset';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiAuthV1PasswordReset' in parameters) {
            params = parameters['apiAuthV1PasswordReset'];
        }
        const name_lower = 'post_ApiAuthV1PasswordReset'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    post_ApiAuthV1Refresh_token(parameters: {
        'apiAuthV1RefreshToken' ? : ApiAuthV1Refresh_token,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/auth/v1/refresh_token';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiAuthV1RefreshToken' in parameters) {
            params = parameters['apiAuthV1RefreshToken'];
        }
        const name_lower = 'post_ApiAuthV1Refresh_token'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    get_ApiConnectionsV1AdwordsAuth(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/adwords/auth';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1AdwordsAuth'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiConnectionsV1AdwordsVarTest(parameters: {
        'var': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/adwords/{var}/test';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1AdwordsVarTest'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiConnectionsV1Adwords(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/adwords/';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1Adwords'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    post_ApiConnectionsV1Adwords(parameters: {
        'apiConnectionsV1Adwords' ? : ApiConnectionsV1Adwords,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/adwords/';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiConnectionsV1Adwords' in parameters) {
            params = parameters['apiConnectionsV1Adwords'];
        }
        const name_lower = 'post_ApiConnectionsV1Adwords'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    get_ApiConnectionsV1AdwordsId(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/adwords/{id}';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1AdwordsId'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    delete_ApiConnectionsV1AdwordsId(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/adwords/{id}';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('id' in parameters) {
            params = parameters['id'];
        }
        const name_lower = 'delete_ApiConnectionsV1AdwordsId'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('DELETE', domain, path, params, auth);
    }

    get_ApiConnectionsV1FbVarTest(parameters: {
        'var': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/fb/{var}/test';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1FbVarTest'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiConnectionsV1Fb(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/fb/';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1Fb'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    post_ApiConnectionsV1Fb(parameters: {
        'apiConnectionsV1Fb' ? : ApiConnectionsV1Fb,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/fb/';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiConnectionsV1Fb' in parameters) {
            params = parameters['apiConnectionsV1Fb'];
        }
        const name_lower = 'post_ApiConnectionsV1Fb'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    get_ApiConnectionsV1FbId(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/fb/{id}';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1FbId'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    delete_ApiConnectionsV1FbId(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/fb/{id}';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('id' in parameters) {
            params = parameters['id'];
        }
        const name_lower = 'delete_ApiConnectionsV1FbId'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('DELETE', domain, path, params, auth);
    }

    get_ApiConnectionsV1Twitter(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/twitter';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1Twitter'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    post_ApiConnectionsV1Twitter(parameters: {
        'apiConnectionsV1Twitter' ? : ApiConnectionsV1Twitter,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/twitter';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiConnectionsV1Twitter' in parameters) {
            params = parameters['apiConnectionsV1Twitter'];
        }
        const name_lower = 'post_ApiConnectionsV1Twitter'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    get_ApiConnectionsV1TwitterId(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/twitter/{id}';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1TwitterId'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    delete_ApiConnectionsV1TwitterId(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/twitter/{id}';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('id' in parameters) {
            params = parameters['id'];
        }
        const name_lower = 'delete_ApiConnectionsV1TwitterId'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('DELETE', domain, path, params, auth);
    }

    get_ApiConnectionsV1User(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/user';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1User'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiConnectionsV1(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiConnectionsV1Id(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/connections/v1/{id}';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiConnectionsV1Id'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiImportsV1AuthGs(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/auth/gs';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiImportsV1AuthGs'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiImportsV1AuthGsStatus(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/auth/gs/status';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiImportsV1AuthGsStatus'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiImportsV1AuthS3Credentials(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/auth/s3/credentials';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiImportsV1AuthS3Credentials'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    post_ApiImportsV1AuthS3(parameters: {
        'apiImportsV1AuthS3' ? : ApiImportsV1AuthS3,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/auth/s3/';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiImportsV1AuthS3' in parameters) {
            params = parameters['apiImportsV1AuthS3'];
        }
        const name_lower = 'post_ApiImportsV1AuthS3'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    post_ApiImportsV1AuthShopify(parameters: {
        'apiImportsV1AuthShopify' ? : ApiImportsV1AuthShopify,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/auth/shopify/';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiImportsV1AuthShopify' in parameters) {
            params = parameters['apiImportsV1AuthShopify'];
        }
        const name_lower = 'post_ApiImportsV1AuthShopify'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    get_ApiImportsV1AuthShopifyStatus(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/auth/shopify/status';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiImportsV1AuthShopifyStatus'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiImportsV1Configs(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/configs';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiImportsV1Configs'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiImportsV1ConfigsId(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/configs/{id}';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiImportsV1ConfigsId'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    post_ApiImportsV1File(parameters: {
        'apiImportsV1File' ? : ApiImportsV1File,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/file/';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiImportsV1File' in parameters) {
            params = parameters['apiImportsV1File'];
        }
        const name_lower = 'post_ApiImportsV1File'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    post_ApiImportsV1Gs(parameters: {
        'apiImportsV1Gs' ? : ApiImportsV1Gs,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/gs/';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiImportsV1Gs' in parameters) {
            params = parameters['apiImportsV1Gs'];
        }
        const name_lower = 'post_ApiImportsV1Gs'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    post_ApiImportsV1S3(parameters: {
        'apiImportsV1S3' ? : ApiImportsV1S3,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/s3/';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiImportsV1S3' in parameters) {
            params = parameters['apiImportsV1S3'];
        }
        const name_lower = 'post_ApiImportsV1S3'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    post_ApiImportsV1Shopify(parameters: {
        'apiImportsV1Shopify' ? : ApiImportsV1Shopify,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/shopify/';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiImportsV1Shopify' in parameters) {
            params = parameters['apiImportsV1Shopify'];
        }
        const name_lower = 'post_ApiImportsV1Shopify'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    get_ApiImportsV1Statuses(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/statuses';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiImportsV1Statuses'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    post_ApiImportsV1Statuses(parameters: {
        'apiImportsV1Statuses' ? : ApiImportsV1Statuses,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/statuses';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiImportsV1Statuses' in parameters) {
            params = parameters['apiImportsV1Statuses'];
        }
        const name_lower = 'post_ApiImportsV1Statuses'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    update_ApiImportsV1Statuses(parameters: {
        'apiImportsV1StatusesUpdate' ? : ApiImportsV1StatusesUpdate,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/statuses';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('apiImportsV1StatusesUpdate' in parameters) {
            params = parameters['apiImportsV1StatusesUpdate'];
        }
        const name_lower = 'update_ApiImportsV1Statuses'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('PATCH', domain, path, params, auth);
    }

    get_ApiImportsV1StatusesId(parameters: {
        'id': string,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/imports/v1/statuses/{id}';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiImportsV1StatusesId'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    get_ApiUsersV1Profile(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/api/users/v1/profile';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_ApiUsersV1Profile'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    post_AuthV1Get_token(parameters: {
        'authV1GetToken' ? : AuthV1Get_token,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/auth/v1/get_token';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('authV1GetToken' in parameters) {
            params = parameters['authV1GetToken'];
        }
        const name_lower = 'post_AuthV1Get_token'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

    get_AuthV1RedirectGs(parameters: {
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/auth/v1/redirect/gs';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }
        for (const key_name in parameters) {
            if (key_name[0] !== '$') {
                params[key_name] = parameters[key_name];
            }
        }

        const name_lower = 'get_AuthV1RedirectGs'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('GET', domain, path, params, auth);
    }

    post_AuthV1Registration(parameters: {
        'authV1Registration' ? : AuthV1Registration,
        $queryParameters ? : any,
        $domain ? : string
    }): Observable < Response > {
        let domain: string;
        let path = '/auth/v1/registration';
        const auth = !(PUBLIC_URLS_LIST.includes(path));
        let params = {};
        if (parameters.$domain) {
            domain = parameters.$domain;
        } else {
            domain = auth ? environment.apiUrl : environment.publicApiUrl
        }
        for (const key_name in parameters) {
            if (path.includes(`{${key_name}}`)) {
                path = path.replace(`{${key_name}}`, parameters[key_name]);
                delete parameters[key_name];
            }
        }

        if ('authV1Registration' in parameters) {
            params = parameters['authV1Registration'];
        }
        const name_lower = 'post_AuthV1Registration'.toLowerCase();

        if (parameters.$queryParameters) {
            params = Object.assign({}, params, parameters.$queryParameters);
        }

        return this.execute('POST', domain, path, params, auth);
    }

}

type SalesApiTransactionItem = {
    'salesAmountNet': string

    'productCoreId': string

    'tax': string

    'orderId': string

    'productIsAddOn': boolean

    'productBrandName': string

    'productType': string

    'eventId': string

    'fulfillmentBy': string

    'statusOrderLine': string

    'salesAmountTax': string

    'orderLineId': string

    'salesQuantity': number

    'productName': string

    'salesAmountDiscount': string

    'productId': string

    'salesAmountTotal': string

    'discount': string

    'salesUnitPrice': string

};
type SalesApiTransaction = {
    'deliveryAddressLine1': string

    'dateScheduledDeliveryDispatch': string

    'customerEmailDomain': string

    'salesPromotionCampaignType': string

    'customerAddress3': string

    'carrier': string

    'paymentId': string

    'deliveryAddressLastName': string

    'salesPromotionCode': string

    'salesPromotionDiscountType': string

    'orderLines': Array < SalesApiTransactionItem >
        | SalesApiTransactionItem

    'eventSubtype': string

    'customerLanguage': string

    'customerCountry': string

    'orderId': string

    'customerAddressType': string

    'deliveryAddressLine2': string

    'businessCountryOfSale': string

    'customerCity': string

    'tsScheduledDeliveryMax': string

    'customerFirstName': string

    'customerAddress2': string

    'customerConcatenatedName': string

    'transactionType': string

    'customerKey': string

    'eventId': string

    'dispatchId': string

    'deliveryAddressCountyState': string

    'salesPromotionMedium': string

    'tsOrderPlaced': string

    'customerId': string

    'dateScheduledDispatch': string

    'deliveryAddressCity': string

    'statusOrder': string

    'customerAddress1': string

    'tsOrderCreated': string

    'carrierServiceName': string

    'customerPostZipCode': string

    'customerEmailAddress': string

    'customerTitle': string

    'businessDivision': string

    'businessName': string

    'businessRegion': string

    'deliveryAddressType': string

    'customerCountyState': string

    'salesPromotionCustomerType': string

    'currencyCode': string

    'deliveryAddressName': string

    'deliveryAddressOrganization': string

    'deliveryAddressPostZipCode': string

    'salesPromotionSource': string

    'salesAmountDiscount': string

    'deliveryAddressCountry': string

    'deliveryAddressLine3': string

    'dispatchLocation': string

    'deliveryAddressFirstName': string

    'customerMobile': string

    'customerSocialHandle': string

    'customerLastName': string

    'salesPromotionName': string

    'salesPromotionCampaign': string

    'statusPayment': string

    'tsScheduledDeliveryMin': string

};
type SalesApiTransactionFlattened = {
    'deliveryAddressLine1': string

    'dateScheduledDeliveryDispatch': string

    'customerEmailDomain': string

    'salesPromotionCampaignType': string

    'customerAddress3': string

    'carrier': string

    'paymentId': string

    'deliveryAddressLastName': string

    'salesPromotionCode': string

    'salesPromotionDiscountType': string

    'orderLineProductCoreId': string

    'eventSubtype': string

    'customerLanguage': string

    'customerCountry': string

    'orderId': string

    'customerAddressType': string

    'deliveryAddressLine2': string

    'orderLineSalesAmountNet': string

    'businessCountryOfSale': string

    'customerCity': string

    'tsScheduledDeliveryMax': string

    'customerFirstName': string

    'customerAddress2': string

    'customerConcatenatedName': string

    'transactionType': string

    'customerKey': string

    'orderLineSalesUnitPrice': string

    'orderLineProductIsAddOn': boolean

    'orderLineProductName': string

    'eventId': string

    'orderLineSalesAmountTotal': string

    'dispatchId': string

    'deliveryAddressCountyState': string

    'salesPromotionMedium': string

    'tsOrderPlaced': string

    'fulfillmentBy': string

    'customerId': string

    'statusOrderLine': string

    'dateScheduledDispatch': string

    'deliveryAddressCity': string

    'statusOrder': string

    'customerAddress1': string

    'tsOrderCreated': string

    'orderLineProductBrandName': string

    'carrierServiceName': string

    'customerPostZipCode': string

    'orderLineProductType': string

    'orderLineId': string

    'customerEmailAddress': string

    'customerTitle': string

    'businessDivision': string

    'businessName': string

    'businessRegion': string

    'orderLineSalesAmountTax': string

    'deliveryAddressType': string

    'customerCountyState': string

    'salesPromotionCustomerType': string

    'orderLineSalesAmountDiscount': string

    'currencyCode': string

    'deliveryAddressName': string

    'deliveryAddressOrganization': string

    'deliveryAddressPostZipCode': string

    'salesPromotionSource': string

    'salesAmountDiscount': string

    'deliveryAddressCountry': string

    'deliveryAddressLine3': string

    'dispatchLocation': string

    'orderLineTax': string

    'orderLineSalesQuantity': number

    'deliveryAddressFirstName': string

    'customerMobile': string

    'customerSocialHandle': string

    'customerLastName': string

    'orderLineProductId': string

    'salesPromotionName': string

    'orderLineDiscount': string

    'salesPromotionCampaign': string

    'statusPayment': string

    'tsScheduledDeliveryMin': string

};
type SalesApiTransactionFlattenedRequest = {
    'transactions': Array < SalesApiTransactionFlattened >
        | SalesApiTransactionFlattened

    'appId': string

};
type SalesApiTransactionRequest = {
    'transactions': Array < SalesApiTransaction >
        | SalesApiTransaction

    'appId': string

};
type SalesApiResponse = {};
type ReportingApiBatchSalesRequest = {
    'queries': Array < ReportingApiSalesRequest >
        | ReportingApiSalesRequest

};
type ReportingApiSalesRequest = {
    'type': "revenue" | "count" | "avg_order_value"

    'accountId': string

    'startingFrom': string

    'endingWith': string

    'aggregateBy': "hour" | "day" | "week" | "month" | "all"

};
type ReportingApiBatchSegmentsRequest = {
    'queries': Array < ReportingApiSegmentsRequest >
        | ReportingApiSegmentsRequest

};
type ReportingApiSegmentsRequest = {
    'type': "cost" | "revenue" | "impressions" | "clicks" | "conversions" | "cpa" | "ctr" | "cr"

    'accountId': string

    'platform': "facebook" | "adwords" | "twitter"

    'segmentIds': Array < string >
        | string

    'startingFrom': string

    'endingWith': string

    'aggregateBy': "hour" | "day" | "week" | "month" | "all"

};
type ReportingApiBatchResponse = {
    'resultset': Array < ReportingApiResponse >
        | ReportingApiResponse

};
type ReportingApiResponse = {
    'data': Array < {
            'timestamp': string

            'value': number

        } >
        | {
            'timestamp': string

            'value': number

        }

};
type ReportingApiSegmentsDetailRequest = {
    'accountId': string

    'segmentIds': Array < string >
        | string

    'startingFrom': string

    'endingWith': string

    'aggregateBy': "hour" | "day" | "week" | "month" | "all"

};
type ReportingApiSegmentsDetailRow = {
    'datetime': string

    'cost': number

    'revenue': number

    'impressions': number

    'clicks': number

    'conversions': number

    'cpa': number

    'ctr': number

    'cr': number

};
type ReportingApiSegmentDetailResponseMapping = {
    'segment1': ReportingApiSegmentsDetailRow

};
type ReportingApiSegmentsDetailResponse = {
    'adwords': ReportingApiSegmentDetailResponseMapping

    'facebook': ReportingApiSegmentDetailResponseMapping

    'twitter': ReportingApiSegmentDetailResponseMapping

};
type ReportingApiUpdateRequest = {
    'accountId': string

    'datasetName': string

    'projectId': string

};
type ReportingApiUpdateResponse = {};
type ApiAuthV1PasswordResetConfirm = {
    'new_password1': string

    'new_password2': string

    'uuid': string

};
type ApiAuthV1PasswordReset = {};
type ApiAuthV1Refresh_token = {};
type ApiConnectionsV1Adwords = {
    'provider': string

    'email': string

    'account_id': string

    'status': string

    'token': string

};
type ApiConnectionsV1Fb = {
    'provider': string

    'email': string

    'account_id': string

    'status': string

    'token': string

};
type ApiConnectionsV1Twitter = {
    'provider': string

    'email': string

    'account_id': string

    'status': string

    'user': string

};
type ApiImportsV1AuthS3 = {};
type ApiImportsV1File = {
    'file': string

};
type ApiImportsV1Gs = {
    'path': string

    'reimport': boolean

    'reimport_type': string

};
type ApiImportsV1Shopify = {
    'shop': string

};
type ApiImportsV1S3 = {
    'path': string

    'reimport': boolean

    'reimport_type': string

};
type ApiImportsV1Statuses = {
    'filter': {
        'import_ids': Array < number >
            | number

    }

};
type ApiImportsV1StatusesUpdate = {
    'verified': boolean

};
type ApiUsersV1ProfileResponse = {
    'password': string

    'last_login': string

    'is_superuser': boolean

    'email': string

    'client_id': string

    'client_secret': string

    'is_active': boolean

    'groups': string

    'user_permissions': string

};
type AuthV1Get_token = {};
type AuthV1Registration = {
    'email': string

    'password': string

    'first_name': string

    'last_name': string

    'account_name': string

    'company': string

};
type ApiImportsV1AuthStatusResponse = {
    'status': "authorized" | "unauthorized"

};
type ApiImportsV1AuthS3CredentialsResponse = {
    'aws_access_key_id': string

    'aws_secret_access_key': string

};
type ApiImportsV1AuthShopifyResponse = {
    'auth_uri': string

};
type ApiImportsV1AuthShopify = {
    'shop': string

};
type ApiConnectionsV1AdwordsId = {
    'id': string
};
type ApiConnectionsV1FbId = {
    'id': string
};
type ApiConnectionsV1TwitterId = {
    'id': string
}
type ApiUsersV1ProfilesId = {
  'id': string
}
