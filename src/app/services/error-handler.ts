import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { TrackerService } from './sp-tracker.service';
import { environment } from '../../environments/environment';
import { Response } from '@angular/http';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
private tracker:TrackerService;

constructor(private injector:Injector) { 
  	setTimeout(() => this.tracker = this.injector.get(TrackerService));
}
  handleError(error) {
    this.tracker.trackEvent('error', null, error.message, null, error);

    // Rethrow the error otherwise it gets swallowed
    throw error;
  }
  
}
