import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/exhaustMap';

import { ApiHttpService } from '../api-http/api-http.service';


const FORM_URL = '/tasks';
const FORM_PATH = '/subscribe';
const RESULTS_URL = '/tasks';
const RESULTS_PATH = '/results';


@Injectable()
export class FormService {
  public id: string;

  constructor(private apiHttpService: ApiHttpService) {
    this.id = '';
  }

  onSubmit(message: string, url: string = FORM_URL, path: string = FORM_PATH): Observable<Response> {
    const params = {
      message: message
    };
    return this.apiHttpService
      .execute('Post', url, path, params);
  }

  onResults(id: string, url: string = RESULTS_URL, path: string = RESULTS_PATH): Observable<Response> {
    const params = {
      id: id
    };
    return Observable
      .timer(0, 5000)
      .exhaustMap(() => {
        console.log('this.onResults().interval(5000).mergeMap()');
        return this.apiHttpService
          .execute('Post', url, path, params);
      });
  }

}
