import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';

import {ApiHttpService} from '../api-http/api-http.service';

@Injectable()
export class ReportService  {

  constructor(
     private apiHttpService: ApiHttpService,

  ) {
  };

  public getSaleChartData(data) {
    const params: any = {};
    params['$queryParameters'] = data;
    return new Promise((resolve, reject) => {
      this.apiHttpService.querySales(params)
        .toPromise()
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          } );
      });
  };

  public getAdvertChartData(data) {

    let promiseArray = data.map((item) => {
      const params: any = {};
      params['$queryParameters'] = item;
      return this.apiHttpService.querySegments( params ).toPromise();
    });

    return new Promise((resolve, reject) => {
      Promise.all(promiseArray).then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });
    });
  };

}

