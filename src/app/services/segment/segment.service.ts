import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {ApiHttpService} from '../api-http/api-http.service';
import { TableRow } from '../../models/segment-data.model';



@Injectable()
export class SegmentService  {
	data: TableRow[];
	reqData = {
      "accountId": "demo",
      "segmentIds": [
      "segment1", "segment2"
      ],
      "startingFrom": "2017-01-18T12:32:53",
      "endingWith": "2017-02-18T12:32:53",
      "aggregateBy": "all"
 }

  constructor(
     private apiHttpService: ApiHttpService,
  ) {}

   public getRows(period) {
     this.reqData.startingFrom = period.startDate;
     this.reqData.endingWith = period.endDate;
    const params: any = {};
    params['$queryParameters'] = this.reqData;
    return this.apiHttpService.querySegmentsDetail(params).toPromise().then((res)=>{
    	return this.buildRowObject(res.json());
    });
  };

  public buildRowObject(data){
    let tableData = [];
    let platforms = Object.keys(data.platforms);
    platforms.forEach((pl)=>{
      let prefix = pl.substring(0,2) + '_';
      for(let key in data.platforms[pl]){
        if(!tableData[key]){
          tableData[key] = {};
          tableData[key].name = key;
        }
        let plData = data.platforms[pl][key][0];
        for(let k in plData){
          let name = prefix + k;
          tableData[key][name] = plData[k];
        }
      }
    });
    let temp = [];
    for(let k in tableData){
      temp.push(tableData[k]);
    }
    let rows:TableRow[] = [];
    
    temp.forEach((row, i)=> {
      let rowObj = new TableRow(
      row.name || '',
      '31-90',
      '2-5',
      '$0-50',
      3040,
      false,
      1010,
      12,
      800,
      row.fa_ctr || 8,
      row.fa_conversions || 3,
      row.fa_cost || 1850,
      row.fa_cpa || 12,
      false,
      999,
      52,
      row.ad_ctr || 13,
      row.ad_conversions || 6,
      row.ad_cost || 4131,
      row.ad_cpa || 8,
      )
      rows.push(rowObj);
    });

    return rows;
  }

}

