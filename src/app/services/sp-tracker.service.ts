import { Injectable, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import { Contexts, ContextData, ErrContextData } from '../models/sp-tracker';

declare var snowplow : any;

@Injectable()
export class TrackerService {
   	contextSchema:string[] = ["iglu:com.fuzzylabs", null, "jsonschema/1-0-0"];

   	constructor(private router: Router){
         let collectorEndpoint = 'services.fuzzylabsresearch.com/tracker/v1/4cf39b2050f44d09a2b86cef9fe7ff47';
   		this.router.events.forEach((event) => {
		    if(event instanceof NavigationEnd) {
		    	this.trackPageView();	
		    }
		  });
   		snowplow('newTracker', 'cf', collectorEndpoint, {
	      appId: 'app.fuzzylabs.com',
	      platform: 'web',
	      encodeBase64: false,
	      respectDoNotTrack: true,
	      forceSecureTracker: true,
	      cookieDomain: '.fuzzylabsresearch.com',
	      sessionCookieTimeout: 3600,
	      post: true,
	      contexts: {
	      performanceTiming: true,
	      webPage: true
	    }});
   	}
   	buildContextData(event, elem, action, typeEl, err?){
   		let location = this.router.url;
   		let elemType:string;
         let elemId:string;
   		let text:string;
   		
   		if(elem && elem.nativeElement){
   			elemType = elem.nativeElement.localName;
   			elemId = elem.nativeElement.id
            text = elem.nativeElement.innerText
   		}
   		if(err){
   			let splitIndex = err.stack ? err.stack.indexOf(':') : null; 
   			let errType = err.stack ? err.stack.substr(0, splitIndex) : typeEl;
   			let data = new ErrContextData(
						errType, 
						location,
						err.url || '', 
						err._body || err.message || err || '',
						err.stack || ''
								
   					);
   			return data;
   		}
   		let data = new ContextData(
						action || '', 
						typeEl || '', 
						location,
						elemType || event.elemType || '',
						elemId || event.elemId || '', 
						event.heading || event.text || text || '',
   					);
   		return data
   	}
   	trackEvent(event, elem, action, typeEl, err?){
   		let schema = this.contextSchema;
   		err ? schema[1] = 'app_error' : schema[1] = 'user_action';
   		let schemaStr = schema.join('/');
   		
   		let contextData = this.buildContextData(event, elem, action, typeEl, err);
   		
   		let contexts = new Contexts(schemaStr, contextData);
   		snowplow('trackStructEvent', '', action, '', '', '', contexts);
   	}

   	trackPageView() {
   		snowplow('trackPageView', '', '', '');
   	}
}