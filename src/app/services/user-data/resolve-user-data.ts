import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { UserDataService } from "./user-data.service";

@Injectable()

export class ResolveUserService implements Resolve<any> {
	profile:any
	constructor(private userDS: UserDataService){}

	resolve(route: ActivatedRouteSnapshot){
		return this.userDS.getData().toPromise();
	}



}