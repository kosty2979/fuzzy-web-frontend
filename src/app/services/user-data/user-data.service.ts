import { Injectable } from '@angular/core';
import { ApiHttpService} from '../api-http/api-http.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserDataService {

  constructor(private api: ApiHttpService) { }

  getData(): Observable<Response> {
    return this.api.get_ApiUsersV1Profile({}).map(res => res = res.json());
  }

}
