export const environment = {
  production: false,
  prefill: false,
  publicApiUrl: 'http://127.0.0.1:8000',
  apiUrl: 'http://127.0.0.1:9000',
  apiVersion: 'v1',
  assetsBaseUrl: '/assets',
  facebookAppId: '1398619830218410',
  loggerLevel: 5
};
