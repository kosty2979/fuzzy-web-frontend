export const environment = {
  production: true,
  prefill: false,
  apiUrl: 'https://services.fuzzylabsresearch.com',
  publicApiUrl: 'https://services.fuzzylabsresearch.com',
  apiVersion: 'v1',
  assetsBaseUrl: '/assets',
  facebookAppId: '406974749684235',
  loggerLevel: 2
};
