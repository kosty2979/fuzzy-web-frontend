var fs = require('fs');
var CodeGen = require('swagger-js-codegen').CodeGen;

var file = 'swagger.json';
var swagger = JSON.parse(fs.readFileSync(file, 'UTF-8'));

var tsSourceCode = CodeGen.getTypescriptCode({ esnext:true, isES6: true, swagger: swagger, isNode: false,      template: {
        class: fs.readFileSync('codegen_templates/typescript-class.mustache', 'utf-8'),
        method: fs.readFileSync('codegen_templates/typescript-method.mustache', 'utf-8'),
        type: fs.readFileSync('codegen_templates/type.mustache', 'utf-8')
    }});


const imports_text = fs.readFileSync('codegen_templates/imports.ts', 'utf-8');
fs.writeFileSync('src/app/services/api-http/api-http.service.ts', tsSourceCode+imports_text);
